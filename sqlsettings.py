#coding: utf-8
""" Docstring """

import config as c
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from random import random
import logging

Base = declarative_base()
session = sessionmaker()
engine = create_engine('mysql://{0}:{1}@{2}/{3}'.format(c.MYSQL_USER,
                                                        c.MYSQL_PASS,
                                                        c.MYSQL_HOST,
                                                        c.PR_DB))
session.configure(bind=engine)
Base.metadata.create_all(engine)
s = session()
cfg = {}


def return_list(table, column):
    """ Returns list with calues from column <column> """
    __table_args__ = {
         "autoload": True,
         "autoload_with": s.bind,
         "useexisting": True
        }
    Row = type(table+str(random()), (Base, ), {'__tablename__':table, '__table_args__':__table_args__})
    result = s.query(getattr(Row, column)).all()
    return [value[0] for value in result]

def return_dictlist(table):
    """ Returns list with values from column <column> """
    __table_args__ = {
         "autoload": True,
         "autoload_with": s.bind,
         "useexisting": True
        }
    Row = type(table+str(random()), (Base, ), {'__tablename__':table, '__table_args__':__table_args__})
    result = s.query(Row).all()
    return [u.__dict__ for u in result]

"""
def return_tuplelist(table):
    return [u.items() for u in return_dictlist(table)]
"""
class ConfigTable(Base):
    """ SQLAlchemy class """
    __tablename__ = c.PR_CONFIG_TABLE
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(250), nullable=False)
    value = Column(String(250), nullable=False)




logging.info("Loading config from sql table")

for kw in s.query(ConfigTable).all():
    cfg[kw.name] = kw.value


DEBUG = True

MYSQL_HOST = c.MYSQL_HOST
MYSQL_USER = c.MYSQL_USER
MYSQL_PASS = c.MYSQL_PASS
MYSQL_DATABASE = c.MYSQL_DATABASE

DB1_TABLE = cfg['db1']
DB2_TABLE = cfg['db2']
THREADS = int(cfg['threads'])
TIMEOUT_BETWEEN_RETRIES = float(cfg['timeout_between_retries'])
NUMBER_OF_RETRIES = int(cfg['number_of_retries'])
REQUEST_TIMEOUT = int(cfg['request_timeout'])

API_URL = cfg['api_url']
API_SEARCH = cfg['api_search']
#API_PATH_EXPORT = cfg['api_path_export']
API_KEY = cfg['api_key']
PAGE = cfg['page']
SPHINX_LIMIT = cfg['sphinx_limit']
SPHINX_LIMIT_PASS = cfg['sphinx_limit_pass']
RELEVANCY_THRESHOLD = cfg['relevancy_threshold']
OUT_DIR = cfg['output_dir_1']
OUT_DIR_DB2 = cfg['output_dir_2']

LOG_DB1_FN = cfg['log_db1_fn']
MERCHS_TABLE = cfg['merchs_table']
MERCHS_TABLE_2 = cfg['merchs_table_2']
BRANDS_TABLE = cfg['brands_table']
BRANDS_TO_REMOVE_TABLE = cfg['brands_to_remove_table']
BRANDS_TO_REPLACE_TABLE = cfg['brands_to_replace_table']
CATEGORY_TABLE = cfg['category_table']

PRICE_FILTER = return_list('price_filter', 'value')

DB1_FILENAME = cfg['db1_filename']
DB2_FILENAME = cfg['db2_filename']


PARSER_RULES = return_dictlist('parser_rules')

MAX_CPC_RULES = return_dictlist('max_cpc_rules')

MERCHANTS_MULTIPLIERS = return_dictlist('merchs_multiplier')

PRICE_RANGE_RULES = return_dictlist('price_range_rules')

REMOVE_DUP_SWITCH = int(cfg['remove_dup_switch']) # used to remove duplicates by 'description' before counting number
                      # of items after API quering in convert.py 1 - remove, 0 - not remove

HEADLINE_V1_NUMBER = int(cfg['headline_v1_number'])
HEADLINE_V1_TEXT = return_list('adword_rules', 'headline_v1_text')

HEADLINE_V2_NUMBER = int(cfg['headline_v2_number'])
HEADLINE_V2_TEXT = return_list('adword_rules', 'headline_v2_text')

DESC_V1_NUMBER = int(cfg['desc_v1_number'])
DESC_V1_TEXT = return_list('adword_rules', 'desc_v1_text')


DESC_V2_NUMBER = int(cfg['desc_v2_number'])
DESC_V2_TEXT = return_list('adword_rules', 'desc_v2_text')



DISPLAY_URL_NUMBER = int(cfg['display_url_number'])
DISPLAY_URL_TEXT= return_list('adword_rules', 'display_url_text')


DESTINATION_URL_NUMBER = int(cfg['dest_url_number'])
DESTINATION_URL_TEXT = return_list('adword_rules', 'dest_url_text')


REMOVE_FROM_KEYWORD_BEFORE_PARSING = return_list('rmv_before_parsing', 'value')

REMOVE_FROM_BRAND = return_list('remove_from_brand', 'value')
REMOVE_FROM_PARSED_KEYWORD = return_list('remove_from_parsed_keyword', 'value')


REPLACE_WORDS = return_dictlist('replace_word')

WORDS_COUNT = int(cfg['words_count'])


FIELDNAMES = return_list('fieldnames', 'values')
FILTER = return_dictlist('filter')
