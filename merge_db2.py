#! /usr/bin/env python

import sqlsettings as cfg
import os
import sys
import csv
import time
import statprof
from collections import defaultdict
import helper
import logging
csv.field_size_limit(sys.maxsize)

def main():
    brands_to_remove = set(helper.return_column_sql(cfg.BRANDS_TO_REMOVE_TABLE, "brand_name"))
    csv_files = [ f for f in os.listdir(cfg.OUT_DIR_DB2) if os.path.isfile(os.path.join(cfg.OUT_DIR_DB2,f)) ]
    if not csv_files:
	logging.critical('No csv files to merge.')
	sys.exit(1)
    header = []
    with open("db2.csv.tmp","w") as fout:
        with open(os.path.join(cfg.OUT_DIR_DB2,csv_files[0])) as fin:
            header_tmp = fin.readline()
            fout.write(header_tmp) # write header

            header =  header_tmp.strip().split(',')
            print header
        for fn in csv_files:
            print fn
            fn = os.path.join(cfg.OUT_DIR_DB2,fn)
            if os.stat(fn).st_size > 0:
                with open(fn) as f:
                    f.next() # skip the header
                    for line in f:
                        fout.write(line)
    with open("db2.csv.tmp", "rb") as fin:
        data = fin.read()
    with open("db2.csv.tmp2", "wb") as fout:
        fout.write(data.replace('\x00', ''))
    os.remove("db2.csv.tmp")

    with open("db2.csv.tmp2","rb") as fin:
        rdr = csv.DictReader(fin, dialect='prosperent')
        with open(cfg.DB2_FILENAME, "w") as fout:
            wtr = csv.DictWriter(fout, cfg.FIELDNAMES, dialect='prosperent')
            wtr.writeheader()
            dupes = set()
            for i,r in enumerate(rdr):
                try:
                    keyword = r['New_Keyword'].strip()
                    if keyword == 'Shoes':
                        print r
                        raise
                except AttributeError as e:
                    print i,r
                    raise e
                brand = r['brand']
                if (keyword not in dupes) and (brand not in brands_to_remove):
                    dupes.add(keyword)
                    try:
                        wtr.writerow(r)
                    except ValueError as e:
                        print r
                        raise e
                else:
                    if (brand in brands_to_remove):
                        print 'Removing brand %s' % keyword
                    else:
                        print 'Found duplicate keyword %s' % keyword

    os.remove("db2.csv.tmp2")


if __name__=="__main__":

    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))
