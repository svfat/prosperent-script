#! /usr/bin/env python
#coding: utf-8

"""
    shopstyle.py
    Author: Stanislav Fateev <ftumane@gmail.com>
"""


import sqlsettings as cfg
import config
import threading
import shopstyle_api
from shopstyle_db import Base, ShRunInfo, ShMerchants, ShCategories, ShPriceRange, ShBrandsData
from Queue import Queue
from collections import defaultdict
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func
from shopstyle_sql import bigsql
from sqlalchemy.sql import text

API_PID = u'uid7684-26173678-40'
BASE_URL = u'api.shopstyle.com'
CATEGORIES_IDS = ["mens-accessories",
                  "mens-clothes",
                  "mens-shoes",
                  "mens-bags",
                  "mens-grooming",
                  "mens-jewelry",
                  "womens-accessories",
                  "womens-clothes",
                  "womens-shoes",
                  "handbags",
                  "womens-beauty",
                  "jewelry",
                  "Home & Living",
                  "kids-and-baby"]
PRICE_RANGE = {'p20:22':'0-50',
               'p23:26':'51-150',
               'p27:29':'151-300',
               'p30:32':'301-500',
               'p33:49':'501+'}

EP = u'/api/v2/' # API endpoint shortener - to use as EP+something in requests
EPH = EP+'products/histogram' # API endpoint for histograms






class Job(threading.Thread):
    """docstring for Job"""
    def __init__(self, q, api, merchants, categories, prices):
        super(Job, self).__init__()
        self.api = api
        self.q = q
        self.merchants = merchants
        self.categories = categories
        self.prices = prices
    def run(self):
        while True:
            try:
                self._run()
            except Exception as e:
                raise e
            finally:
                self.q.task_done()

    def _run(self):
        def make_call(cat):
            response = self.api.q(endpoint=EPH,
                                  pid=API_PID,
                                  filters=u'Retailer',
                                  fts=brand,
                                  cat=cat)
            for r in response['retailerHistogram']:
                self.merchants[brand][r['name']]['value'] += r['count']
            response = self.api.q(endpoint=EPH,
                                  pid=API_PID,
                                  filters=u'Retailer',
                                  fts=brand,
                                  cat=cat, fl=u'd0')
            for r in response['retailerHistogram']:
                self.merchants[brand][r['name']]['value_onsale'] += r['count']
            for price in PRICE_RANGE.keys():
                response = self.api.q(endpoint=EPH,
                                      pid=API_PID,
                                      filters=u'Retailer',
                                      fts=brand,
                                      cat=cat,
                                      fl=price)
                for r in response['retailerHistogram']:
                    self.prices[brand][PRICE_RANGE[price]] += r['count']

        def make_call_categories(cat):
            response = self.api.q(endpoint=EPH,
                                  pid=API_PID,
                                  filters=u'Category',
                                  fts=brand,
                                  cat=cat)
            for r in response['categoryHistogram']:
                if r['id'] in CATEGORIES_IDS:
                    self.categories[brand][r['id']] += r['count']

        brand = self.q.get()
        map(make_call, ['men', 'women', 'kids-and-baby', 'living'])
        map(make_call_categories, ['men', 'women'])


def main():
    """
        Main function

    """
    db_name = config.SH_DB
    engine = create_engine('mysql://{0}:{1}@{2}/{3}'.format(config.MYSQL_USER,
                                                            config.MYSQL_PASS,
                                                            config.MYSQL_HOST,
                                                            db_name))

    session = sessionmaker()
    session.configure(bind=engine)
    Base.metadata.create_all(engine)
    s = session()

    run = ShRunInfo(status="started")
    s.add(run)
    s.commit()
    print "getting brands"
    api = shopstyle_api.api(apiurl=BASE_URL, timeout=3, retries=10)
    response = api.q(endpoint=EP+u'brands', pid=API_PID)
    brands = [brand['name'].encode('utf8') for brand in response['brands']][:100]
    print "start api calls"
    q = Queue()
    merchants = defaultdict(lambda: defaultdict(lambda: {'value': 0, 'value_onsale': 0}))
    categories = defaultdict(lambda: {k:0 for k in CATEGORIES_IDS})
    prices = defaultdict(lambda: {k:0 for k in PRICE_RANGE.values()})
    thread_count = min(cfg.THREADS, len(brands)) # to create not more threads than brands count
    for i in xrange(thread_count): # pylint: disable=W0612
        job = Job(q=q, api=api, merchants=merchants, categories=categories, prices=prices)
        job.setDaemon(True)
        job.start()
    try:
        for brand in brands:
            q.put(brand)
        q.join()

    except KeyboardInterrupt as e:
        raise e
    print "start insert merchants"
    insert_data = []
    for brand in merchants:
        for merch in merchants[brand].keys():
            insert_data.append({'brand': brand,
                                'merchant': merch,
                                'value': merchants[brand][merch]['value'],
                                'value_onsale': merchants[brand][merch]['value_onsale'],
                                'run_id':run.id}) # pylint: disable=E1101

    s.execute(ShMerchants.__table__.insert(), insert_data) # pylint: disable=E1101
    s.commit()
    print "start insert categories"
    insert_data = []
    for category in categories:
        insert_data.append({
            'brand': category,
            'mens_accessories': categories[category].get('mens-accessories', 0),
            'mens_clothes': categories[category].get('mens-clothes', 0),
            'mens_shoes': categories[category].get('mens-shoes', 0),
            'mens_bags': categories[category].get('mens-bags', 0),
            'mens_grooming': categories[category].get('mens-grooming', 0),
            'mens_jewelry': categories[category].get('mens-jewelry', 0),
            'womens_accessories': categories[category].get('womens-accessories', 0),
            'womens_clothes': categories[category].get('womens-clothes', 0),
            'womens_shoes': categories[category].get('womens-shoes', 0),
            'women_bags': categories[category].get('handbags', 0),
            'womens_beauty': categories[category].get('womens-beauty', 0),
            'home': categories[category].get('Home & Living', 0),
            'kids_and_baby': categories[category].get('kids-and-baby', 0),
            'women_jewelry': categories[category].get('jewelry', 0),
            'run_id': run.id # pylint: disable=E1101
            })
    s.execute(ShCategories.__table__.insert(), insert_data) # pylint: disable=E1101
    s.commit()
    print "start insert prices"
    insert_data = []
    for price in prices:
        insert_data.append({
            'brand': price,
            'range_0_50': prices[price].get('0-50', 0),
            'range_51_150': prices[price].get('51-150', 0),
            'range_151_300': prices[price].get('151-300', 0),
            'range_301_500': prices[price].get('301-500', 0),
            'range_501_more': prices[price].get('501+', 0),
            'run_id': run.id # pylint: disable=E1101
            })
    s.execute(ShPriceRange.__table__.insert(), insert_data) # pylint: disable=E1101
    s.commit()


    print "import stored procedure" #possible errors
    for sql in bigsql:
        result = s.execute(text(sql))
        s.commit()
    print "start big procedure" #possible errors
    l = len(brands)
    for i, brand in enumerate(brands):
        print "%d/%d" % (i+1,l)
        sql = text("CALL DOROW(:brandname,:runid);")
        result = s.execute(sql, params={"runid":run.id, "brandname":brand}) # pylint: disable=E1101
    run.end_script = func.now()
    run.status = "finished"
    s.commit()

if __name__ == '__main__':
    main()
