#! /usr/bin/env python

import sys
import time
import sqlsettings as cfg
import os
import shutil
import helper
from convert_csv import Converter
from convert_csv import _write_to_file as wtf
import logging
logger = logging.getLogger(__name__)

def main():

    logname = 'stage2.log'
    logfn = os.path.join(os.curdir, 'log', logname)
    handl = logging.FileHandler(filename = logfn, mode = 'w')
    fmt = logging.Formatter(u'%(levelname)-8s [%(asctime)s] %(message)s')
    handl.setFormatter(fmt)
    handl.setLevel(logging.INFO)
    logger.addHandler(handl)
    logger.setLevel(logging.INFO)
    logger.info("Stage 2 started")
    lock = helper.Lock('stage2')
    if lock.islocked():
        logger.critical('Already running. Lock file is present')
        sys.exit(1)
    lock.lock()

    if os.path.exists(cfg.OUT_DIR_DB2):
        shutil.rmtree(cfg.OUT_DIR_DB2)
    if not os.path.exists(cfg.OUT_DIR_DB2):
        os.makedirs(cfg.OUT_DIR_DB2)
    # read category file to list of dictionaries
    categories = helper.return_dict_sql(cfg.CATEGORY_TABLE)
    tmp_merchants = helper.return_dict_sql(cfg.MERCHS_TABLE)
    merchants = {}
    for d in tmp_merchants:
        merchants[d['name'].lower()] = d['prosperent_id']
    merchants_query = helper.return_column_sql(cfg.MERCHS_TABLE_2, 'prosperent_id')




    # recoding brands_to_replace table
    brands_to_replace_tmp_dict = helper.return_dict_sql(cfg.BRANDS_TO_REPLACE_TABLE)
    brands_to_replace = {}
    for r in brands_to_replace_tmp_dict:
        brands_to_replace[r['brand_name'].lower()] = r['replace_with'].lower()
    csv_files = helper.get_files_by_size(cfg.OUT_DIR, reverse=True)
    i = len(csv_files)
    converter = Converter(categories=categories,merchants=merchants,merchants_query=merchants_query,brands_to_replace=brands_to_replace)
    for (j, csv_file) in enumerate(csv_files):
        bn = os.path.split(csv_file)[1].replace('.csv','')
        logger.info("processing %s (%d/%d)" % (bn, j, i))

        output = converter.convert(bn)
        wtf(os.path.join(cfg.OUT_DIR_DB2, bn+'.csv'), output)
    logger.info("Starting csv files merge process")
    import merge_db2
    merge_db2.main()
    logger.info("Merging process finished")
    logger.info("Starting db2 upload")
    import populate_db2
    populate_db2.main()
    logger.info("Finished db2 upload")
    lock.unlock()
    logger.info("Stage 2 finished. Exiting")
if __name__ == "__main__":
    start_time = time.time()
    if cfg.DEBUG:
        try:
            main()
        finally:
            print "--- %s seconds ---" % (time.time() - start_time)
    else:
        main()

