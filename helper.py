#coding: utf-8
""" Little helpy functions for Prosperent Parser """

import csv
import os
import shutil
from collections import defaultdict
from time import time
from sqlsettings import return_list, return_dictlist, REPLACE_WORDS
#from sqlsettings import return_tuplelist

def return_column_csv(filename, column_name, delimiter=','):
    """ Read csv file and return list with column_name """
    columns = defaultdict(list)
    with open(filename) as f:
        reader = csv.DictReader(f, delimiter=delimiter)
        for row in reader:
            for (k, v) in row.items():
                try:
                    columns[k].append(v.strip())
                except AttributeError:
                    print v
                    raise AttributeError
    return columns[column_name]

def return_column_sql(tablename, column_name):
    return return_list(tablename, column_name)

def return_dict_sql(tablename):
    return return_dictlist(tablename)

"""
def return_tuple_sql(tablename):
    return return_tuplelist(tablename)
"""
def r_commas(string):
    """ Remove ',' and '"' from string """
    try:
        return string.replace(",", " ").replace(";", " ").encode('utf8')
    except AttributeError:
        return ''

def clean_dir(directory):
    """ Remove specified folder and create it again"""
    if os.path.exists(directory):
        shutil.rmtree(directory)
    if not os.path.exists(directory):
        os.makedirs(directory)

class Timer(object):
    """ Smiple timer, remember's time when created,
        returning working time with Timer.time() """
    def __init__(self):
        super(Timer, self).__init__()
        self.start_time = time()
    def time(self):
        """ return working time """
        return time() - self.start_time

def get_files_by_size(folder, reverse=False):
    """ Return list of file paths in directory sorted by file size """

    # Get list of files
    filepaths = []
    for basename in os.listdir(folder):
        filename = os.path.join(folder, basename)
        if os.path.isfile(filename):
            filepaths.append(filename)

    # Re-populate list with filename, size tuples
    for i in xrange(len(filepaths)):
        filepaths[i] = (filepaths[i], os.path.getsize(filepaths[i]))

    # Sort list by file size
    # If reverse=True sort from largest to smallest
    # If reverse=False sort from smallest to largest
    filepaths.sort(key=lambda filename: filename[1], reverse=reverse)

    # Re-populate list with just filenames
    for i in xrange(len(filepaths)):
        filepaths[i] = filepaths[i][0]

    return filepaths

def replace_words(s):
    for row in REPLACE_WORDS:

        s = s.replace(row['in'],row['out'])
    return s

class Lock:
    def __init__(self, lockname):
        self.lockname = os.path.abspath(os.path.join(os.curdir, 'lock', lockname))
    def islocked(self):
        return os.path.isfile(self.lockname)
    def lock(self):
        if not self.islocked():
            open(self.lockname, 'a').close()
        else:
            raise
    def unlock(self):
        if self.islocked():
            os.remove(self.lockname)
        else:
            raise
