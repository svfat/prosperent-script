#! /usr/bin/env python
""" Merging all files in OUT_DIR in one, for exporting to DB1 """

import sqlsettings as cfg
import os
import time
import logging
def main():
    """ Main function - search for csv files in OUT_DIR, merge they and write into DB1_FILENAME """
    csv_files = sorted(
        [f for f in os.listdir(cfg.OUT_DIR) if os.path.isfile(os.path.join(cfg.OUT_DIR, f))])

    with open(cfg.DB1_FILENAME, "w") as fout:
        line = 'brand,merchant,productId,keyword,description,category,price,price_sale\n'
        fout.write(line)
        for fn in csv_files:
            logging.info("Merging file %s" % fn)
            fn = os.path.join(cfg.OUT_DIR, fn)
            with open(fn) as f:
                try:
                    f.next() # skip the header
                except StopIteration:
                    continue
                for line in f:
                    fout.write(line)

if __name__ == "__main__":
    start_time = time.time()
    main()
    print "--- %s seconds ---" % (time.time() - start_time)
