#! /usr/bin/env python
#coding: utf-8

"""
    kwtool.py - automatic campaign builder
    Author: Stanislav Fateev <ftumane@gmail.com>
"""
from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker
import config
import string
import sys
import os
import helper
import logging
from kwtoolconfig import *
logger = logging.getLogger(__name__)




_ = lambda x: string.capwords(x)



def d(s):
    l = s.query(Campaigns).all()
    for i in l:
        print i.adgroup


def replace_line(formats, length, ad, brand, pref, keyword, omitwords=None, debug=False):
    if debug:
        print formats, length, ad, brand, pref, keyword, omitwords
    def _pref(ad, lst, leng, maxlen):
        """return preffered word"""
        ad = ad.split()
        for pref in lst:
            for ad_word in ad:
                if ad_word in pref:
                    if maxlen < leng+len(pref):
                        continue
                    pref_spl = pref.split()
                    wrong = False
                    for pref_word in pref_spl:
                        if pref_word not in ad:
                            wrong = True
                            break
                    if wrong:
                        continue
                    else:
                        return pref
        return '%ERROR%'



    for item in formats:
        if debug: print "*** %s" % item
        tmp = item
        if omitwords:
            ad = ' %s ' % ad
            for ow in omitwords:
                ad = ad.replace(' %s ' % ow, ' ')
            ad.strip()
        if '$brand$' in tmp:
            tmp = tmp.replace('$brand$', brand).replace("  ", " ")
        if '$ad$' in tmp:
            tmp = tmp.replace('$ad$', ad).replace("  ", " ")
            if debug: print "*$ad$ %s" % tmp
        if '$keyword$' in tmp:
            tmp = tmp.replace('$keyword$', keyword).replace("  ", " ")
            if debug: print "*$keywords$ %s" % tmp
        if '$brandnospaces$' in tmp:
            tmp = tmp.replace('$brandnospaces$', brand.replace(" ", "")).replace("  ", " ")
            if debug: print "*$brandnospaces$ %s" % tmp
        if '$ad*$' in tmp:
            tmp2 = tmp.replace('$ad*$', ad).replace("  ", " ")
            while not len(tmp2) <= length:
                tmp2 = tmp.replace('$ad*$', remove_last(ad))
            tmp = tmp2
            if debug: print "*$ad*$ %s" % tmp
        if '$pref$' in tmp:
            tmp = tmp.replace('$pref$', _pref(ad, pref, len(tmp)-len('$pref$'), length)).replace("  ", " ")
            if debug: print "*$pref$ %s" % tmp
        if '$brand_dash$' in tmp:
            tmp = tmp.replace('$brand_dash$', brand.replace(' ', '-')).replace("  ", " ")
        if ('%ERROR%' not in tmp) and (len(tmp) <= length):
            if debug: print tmp
            return tmp

    return "%Error format%"


def replace_first_and_last(st, first, last):
    st = st.lower().split()
    for w in first:
        if w.lower() in st:
            st.insert(0, st.pop(st.index(w.lower())))
    lastindex = len(st)-1
    for w in last:
        if w.lower() in st:
            st.insert(lastindex, st.pop(st.index(w.lower())))
    return " ".join(st)

def find_key(splitted_list, brand_id, words_list):
    result = []
    try:
        for key in splitted_list[brand_id]:
            if splitted_list[brand_id][key] == words_list:
                result.append(key)
    except KeyError as e:
        return result
    return result

#[key for key in splitted_list[camp.brand_id] if splitted_list[camp.brand_id][key] == words_list]

def searchandreplace(camp, replacelist, wordsfirstlist, wordslastlist, splitted_list,
                         websitelist,
                         headlinev1_frm,
                         headlinev2_frm,
                         descv1_frm,
                         descv2_frm,
                         dispurl_frm,
                         desturl_frm,
                         prefwords,
                         omitwords,
                         headlinev1_len,
                         headlinev2_len,
                         descv1_len,
                         descv2_len,
                         desturl_len,
                         dispurl_len, brand_id_dict,numb_of_search):
    campadgroup = camp.adgroup

    for wordpair in replacelist:
        campadgroup = campadgroup.replace(wordpair[0], wordpair[1])
    campadgroup = replace_first_and_last(campadgroup, wordsfirstlist, wordslastlist)
    words_list = set(campadgroup.lower().split())
    chose = find_key(splitted_list, camp.brand_id, words_list)
    # search for keyword with max value
    if chose:
        chosen = (None, 0)
        for key in chose:
            if chosen[1] < numb_of_search[key][0]:
                chosen = (key, numb_of_search[key][0])
        campadgroup = chosen[0]

    for word in websitelist:
        if word in camp.keyword:
            campadgroup = 'website'
            break
    try:
        ad = campadgroup.strip()
    except AttributeError:
        logger.error("adgroup Attribute Error")
        return
    if campadgroup == 'website':
        ad = ""
    brand = brand_id_dict[camp.brand_id]
    keyword = camp.keyword.strip().replace(' ', '%20')

    camp.headline1 = _(replace_line(formats=headlinev1_frm,
                                  length=headlinev1_len,
                                  ad=ad,
                                  brand=brand,
                                  pref=prefwords,
                                  keyword=keyword,
                                  omitwords=omitwords))
    camp.headline2 = _(replace_line(formats=headlinev2_frm,
                                  length=headlinev2_len,
                                  ad=ad,
                                  brand=brand,
                                  pref=prefwords,
                                  keyword=keyword,
                                  omitwords=omitwords))
    camp.descline1 = _(replace_line(formats=descv1_frm,
                                  length=descv1_len,
                                  ad=ad,
                                  brand=brand,
                                  pref=prefwords,
                                  keyword=keyword,
                                  omitwords=omitwords))
    camp.descline2 = _(replace_line(formats=descv2_frm,
                                  length=descv2_len,
                                  ad=ad,
                                  brand=brand,
                                  pref=prefwords,
                                  keyword=keyword,
                                  omitwords=omitwords))
    camp.displayurl = replace_line(formats=dispurl_frm,
                                  length=dispurl_len,
                                  ad=ad,
                                  brand=brand,
                                  pref=prefwords,
                                  keyword=keyword)
    camp.desturl = replace_line(formats=desturl_frm,
                                  length=desturl_len,
                                  ad=ad,
                                  brand=brand,
                                  pref=prefwords,
                                  keyword=keyword)
    camp.keyword = camp.keyword.strip()
    if not campadgroup:
        campadgroup = brand
    camp.adgroup = _(campadgroup.strip())


def main():
    """ Main function """
    try:
        db_name = sys.argv[1]
    except IndexError:
        print "Usage: %s <dbname>" % sys.argv[0]
        sys.exit(1)

    logname = 'kwtool-%s.log' % db_name
    logfn = os.path.join(os.curdir, 'log', logname)
    handl = logging.FileHandler(filename = logfn, mode = 'w')
    fmt = logging.Formatter(u'%(levelname)-8s [%(asctime)s] %(message)s')
    handl.setFormatter(fmt)
    handl.setLevel(logging.INFO)
    logger.addHandler(handl)
    logger.setLevel(logging.INFO)
    lock = helper.Lock('kwtool-%s' % db_name)
    if lock.islocked():
        logger.critical('Already running. Lock file is present')
        sys.exit(1)
    lock.lock()
    logger.info("Kwtool started")
    engine = create_engine('mysql://{0}:{1}@{2}/{3}'.format(config.MYSQL_USER,
                                                            config.MYSQL_PASS,
                                                            config.MYSQL_HOST,
                                                            db_name))

    session = sessionmaker()
    session.configure(bind=engine)
    Base.metadata.create_all(engine)
    s = session()
    logger.info("Loading config")
    cfg = {}
    for kw in s.query(ConfigTable).all():
        cfg[kw.name] = kw.value

    replacelist = [(' %s ' % c.a, ' %s ' % c.b) for c in s.query(WordsToReplace).all()]
    websitelist = [c.word for c in s.query(WordsWebsite).all()]
    wordsfirstlist = [c.word for c in s.query(WordsFirst).all()]
    wordslastlist = [c.word for c in s.query(WordsLast).all()]
    formats = [c for c in s.query(AdFormats).all()]
    headlinev1_frm = [c.headline_v1 for c in formats if c.headline_v1]
    headlinev2_frm = [c.headline_v2 for c in formats if c.headline_v2]
    descv1_frm = [c.desc_v1 for c in formats if c.desc_v1]
    descv2_frm = [c.desc_v2 for c in formats if c.desc_v2]
    dispurl_frm = [c.displayurl for c in formats if c.displayurl]
    desturl_frm = [c.desturl for c in formats if c.desturl]
    prefwords = [c.word for c in s.query(PrefWords).all()]
    omitwords = [c.word for c in s.query(OmitWords).all()]
    headlinev1_len = int(cfg['adf_headlinev1_len'])
    headlinev2_len = int(cfg['adf_headlinev2_len'])
    descv1_len = int(cfg['adf_descv1_len'])
    descv2_len = int(cfg['adf_descv2_len'])
    desturl_len = int(cfg['adf_desturl_len'])
    dispurl_len = int(cfg['adf_dispurl_len'])


    keywords = s.query(Keywords).all()

    logger.info("Generating number of searches table")
    numb_of_search = {}
    for i,kw in enumerate(keywords):
        logger.info("Generating table for %s | %d" % (kw.keyword, i))
        numb_of_search[kw.keyword.lower()] = (kw.search_volume, kw.brand_id)
    splitted_list = {}
    for i,kw in enumerate(numb_of_search.keys()):
        logger.info("Generating number of searches for %s | %d" % (kw,i))
        brand_id = numb_of_search[kw][1]
        if brand_id not in splitted_list.keys():
            splitted_list[brand_id] = dict()
        splitted_list[brand_id][kw] = set(kw.split())

    logger.info("Clearing campaigns table")
    s.query(Campaigns).delete()
    logger.info("Cleared")
    logger.info("Creating campaigns")



    insert_data = [{'brand_id': kw.brand_id,
                    'keyword': ' {0} '.format(kw.keyword.lower()),
                    'adgroup': ' {0} '.format(kw.keyword.lower()),
                    'campaign': kw.brand_id,
                    'number_of_searches': kw.search_volume} for kw in keywords]

    s.execute(Campaigns.__table__.insert().prefix_with("IGNORE"), insert_data)

    logger.info("Copying keywords to adgroup")
    s.commit()
    logger.info("There are {0} items to convert".format(len(s.query(Campaigns).all())))
    wordstoremove = [c.word for c in s.query(WordsToRemove).all()]
    delete_list = []

    t = text("CREATE INDEX keyword_index ON %s (keyword)" % config.KW_CAMPAIGNS_TABLE)
    try:
      s.execute(t)
    except:
      logger.info('Cannot create index')

    for word in wordstoremove:
        delete_list += s.query(Campaigns).filter(
            Campaigns.keyword.like('% {0} %'.format(word))
            ).all()
    for item in delete_list:
        logger.info("Removing {0} because it is in words to remove".format(item.keyword))
        s.delete(item)
    logger.info("Commiting")
    s.commit()


    logger.info("Finished. There are {0} items to convert".format(len(s.query(Campaigns).all())))
    logger.info("Removing brand names from 'adgroup'")

    brand_id_dict = {}
    for brand in s.query(Brands).all():
        brand_id_dict[brand.id] = brand.brand_name.lower()

    for camp in s.query(Campaigns).all():
        logger.info("Removing brand from %s" % camp.adgroup)
        try:
            camp.adgroup = camp.adgroup.replace(brand_id_dict[camp.brand_id]," ").replace("  ", " ")
        except KeyError:
            logger.error("No such brand %s" % camp.brand_id)
    logger.info("Commiting")
    s.commit()
    logger.info("Finished. There are {0} items to convert".format(len(s.query(Campaigns).all())))





    logger.info("search and replace in 'adgroup'")
    for i,camp in enumerate(s.query(Campaigns).all()):
        logger.info("search for %s brand_id %s | %d" % (camp.adgroup, camp.brand_id, i))
        searchandreplace(camp,
                         replacelist,
                         wordsfirstlist,
                         wordslastlist,
                         splitted_list,
                         websitelist,
                         headlinev1_frm,
                         headlinev2_frm,
                         descv1_frm,
                         descv2_frm,
                         dispurl_frm,
                         desturl_frm,
                         prefwords,
                         omitwords,
                         headlinev1_len,
                         headlinev2_len,
                         descv1_len,
                         descv2_len,
                         desturl_len,
                         dispurl_len,
                         brand_id_dict,
                         numb_of_search)
        if i % 10000 == 0:
            logger.info("Commiting")
            s.commit()

    logger.info("Commiting")

    s.commit()
    lock.unlock()
    logger.info("Kwtool finished")


if __name__ == "__main__":
    main()
