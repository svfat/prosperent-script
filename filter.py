#! /usr/bin/env python
import sqlsettings as cfg
import helper


def main():


    table = cfg.DB2_TABLE

    __table_args__ = {
         "autoload": True,
         "autoload_with": cfg.s.bind,
         "useexisting": True
        }
    Row = type(table, (cfg.Base, ), {'__tablename__':table, '__table_args__':__table_args__})



    print "resetting upload status"


    cfg.s.query(Row).update({Row.To_upload: 0})
    cfg.s.commit()


    li = cfg.FILTER
    ffilter = "`%s` %s '%s'" % (li[0]['field_name'],li[0]['operation'],li[0]['value'])
    for obj in li[1:]:
        ffilter += " AND `%s` %s '%s'" % (obj['field_name'],obj['operation'],obj['value'])



    for rule in cfg.FILTER:
        ffilter
        print rule['field_name'],rule['operation'],rule['value']

    ffilter = "`%s` %s '%s' " % (rule['field_name'],rule['operation'],rule['value'])
    #result = cfg.s.query(Row).filter(ffilter)

    #l = len(result.all())

    #print "found %d values - saving" % l
    #for c in result.all():
    #    c.To_upload = 1
    print "marking items to upload"
    cfg.s.query(Row).filter(ffilter).update({Row.To_upload: 1}, synchronize_session='fetch')

    cfg.s.commit()

if __name__ == '__main__':
    main()
