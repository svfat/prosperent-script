#! /usr/bin/env python
""" Config file, you can use it with 'import config as cfg' in your code """
import os
import csv

csv.register_dialect('prosperent',quotechar='~',quoting=csv.QUOTE_ALL)
#dialect = csv.Dialect()
#dialect.quotechar="~"
#print csv.QUOTE_ALL
#dialect.quoting=csv.QUOTE_ALL

MYSQL_HOST = 'localhost'
MYSQL_USER = 'root'
MYSQL_PASS = '123456a'
MYSQL_DATABASE = 'prosperent_products_db'
DB1_TABLE = 'db1'
DB2_TABLE = 'db2'


KW_DB = 'kw_aw_campaigns_db'
KW_CONFIG_TABLE = 'kwtool_config'
KW_BRANDS_TABLE = 'brands'
KW_KEYWORDS_TABLE = 'keywords'
KW_BRANDS_COL_BRAND_NAME = 'brand_name'
KW_KEYWORDS_COL_BRAND_ID = 'brand_id'
KW_KEYWORDS_COL_KEYWORD = 'keyword'
KW_CAMPAIGNS_TABLE = 'campaigns'
KW_WORDSTOREMOVE_TABLE = 'wordstoremove'
KW_WORDSTOREPLACE_TABLE = 'wordstoreplace'
KW_WORDSWEBSITE_TABLE = 'website'
KW_ADFORMATS_TABLE = 'ad_format'
KW_PREFWORDS_TABLE = 'prefwords'
KW_OMITWORDS_TABLE = 'omitwords'
KW_WORDSFIRST_TABLE = 'wordsfirst'
KW_WORDSLAST_TABLE = 'wordlast'
KW_MERCHANTS_TABLE = 'merchants'

SH_DB = 'shopstyle_research'
#SH_DB = 'tmp'
SH_CONFIG_TABLE = 'sh_config'
SH_BRANDS_TABLE = 'brands_merchants'
SH_BRANDSDATA_TABLE = 'brandsdata'
SH_CATEGORIES_TABLE = 'brandscategories'
SH_PRICERANGE_TABLE = 'pricerange'
SH_RUNINFO_TABLE = 'script_run_info'

PR_DB = 'prosperent_products_db'
PR_CONFIG_TABLE = 'config'


try:
    from local_config import *
except ImportError as e:
    pass
