<?php
$usage = "Usage: /start.php?script=stage1<br>/start.php?script=stage2<br>/start.php?script=reset<br>";
$workdir = "/usr/local/prosperent-script";

if (isset($_GET['script'])) {
    $script = $_GET['script'];
    }
else {
    echo $usage;
    die();
    }
switch ($script) {
    case "stage1":
        exec("cd ".$workdir."; ./stage1.sh > /dev/null 2>/dev/null &");
	    echo("Started stage1");
        break;
    case "stage2":
        exec("cd ".$workdir."; ./stage2.sh > /dev/null 2>/dev/null &");
	    echo("Started stage2");
        break;
    case "kwtool-kw-aw":
        exec("cd ".$workdir."; ./kwtool-kw_aw_campaigns_db.sh > /dev/null 2>/dev/null &");
        echo("Started kwtool kw_aw_campaigns_db");
        break;
    case "kwtool-second":
        exec("cd ".$workdir."; ./kwtool-second_kw_aw.sh > /dev/null 2>/dev/null &");
        echo("Started kwtool second_kw_aw");
        break;
    case "shopstyle":
        exec("cd ".$workdir."; ./shopstyle.sh > /dev/null 2>/dev/null &");
        echo("Started shopstyle research tool");
        break;
    case "reset":
        exec("cd ".$workdir."; ./reset.sh > /dev/null 2>/dev/null &");
        echo("Reset complete");
        break;
    default:
        echo $usage;
}
