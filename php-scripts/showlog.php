<?php

	function tailCustom($filepath, $lines = 1, $adaptive = true) {

		// Open file
		$f = @fopen($filepath, "rb");
		if ($f === false) return false;

		// Sets buffer size
		if (!$adaptive) $buffer = 4096;
		else $buffer = ($lines < 2 ? 64 : ($lines < 10 ? 512 : 4096));

		// Jump to last character
		fseek($f, -1, SEEK_END);

		// Read it and adjust line number if necessary
		// (Otherwise the result would be wrong if file doesn't end with a blank line)
		if (fread($f, 1) != "\n") $lines -= 1;

		// Start reading
		$output = '';
		$chunk = '';

		// While we would like more
		while (ftell($f) > 0 && $lines >= 0) {

			// Figure out how far back we should jump
			$seek = min(ftell($f), $buffer);

			// Do the jump (backwards, relative to where we are)
			fseek($f, -$seek, SEEK_CUR);

			// Read a chunk and prepend it to our output
			$output = ($chunk = fread($f, $seek)) . $output;

			// Jump back to where we started reading
			fseek($f, -mb_strlen($chunk, '8bit'), SEEK_CUR);

			// Decrease our line counter
			$lines -= substr_count($chunk, "\n");

		}

		// While we have too many lines
		// (Because of buffer size we might have read too many)
		while ($lines++ < 0) {

			// Find first newline and remove all text before that
			$output = substr($output, strpos($output, "\n") + 1);

		}

		// Close file and return
		fclose($f);
		return trim($output);

	}




$workdir = "/usr/local/prosperent-script";
$usage = "Usage: /showlog.php?script=stage1\n\r/showlog.php?script=stage2\n\r";

if (isset($_GET['script'])) {
    $script = $_GET['script'];
    }
else {
    die($usage);
    }
switch ($script) {
    case "stage1":
        $output = tailCustom($workdir."/log/stage1.log",30);
        break;
    case "stage2":
        $output = tailCustom($workdir."/log/stage2.log",30);
        break;
    case "kwtool-kw-aw":
        $output = tailCustom($workdir."/log/kwtool-kw_aw_campaigns_db.log",30);
        break;
    case "kwtool-second":
        $output = tailCustom($workdir."/log/kwtool-second_kw_aw.log",30);
        break;
    default:
        die($usage);

}
echo nl2br($output);
