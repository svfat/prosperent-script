#coding: utf-8

"""
    shopstyle_db.py
    Author: Stanislav Fateev <ftumane@gmail.com>
"""
import config
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func

run_id_key = config.SH_RUNINFO_TABLE+'.id'
#brand_key = config.SH_BRANDS_TABLE+'.brand'

class Common(object):
    __table_args__ = {'mysql_charset': 'utf8', 'mysql_engine': 'InnoDB'}
    id = Column(Integer, primary_key=True, autoincrement=True)


Base = declarative_base(cls=Common)


class ShRunInfo(Base):
    __tablename__ = config.SH_RUNINFO_TABLE
    start_script = Column(DateTime(timezone=True), default=func.now())
    end_script = Column(DateTime)
    status = Column(String(255))

class ShMerchants(Base):
    """ SQLAlchemy class """
    __tablename__ = config.SH_BRANDS_TABLE
    #id_row = Column(Integer)
    #id_cat = Column(Integer)
    brand = Column(String(255), index=True, nullable=False)
    merchant = Column(String(250), nullable=False)
    value = Column(Integer)
    value_onsale = Column(Integer)
    run_id = Column(Integer, ForeignKey(run_id_key))

class ShCategories(Base):
    """ SQLAlchemy class """
    __tablename__ = config.SH_CATEGORIES_TABLE
    brand = Column(String(255), index=True)
    mens_accessories = Column(Integer)
    mens_clothes = Column(Integer)
    mens_shoes = Column(Integer)
    mens_bags = Column(Integer)
    mens_grooming = Column(Integer)
    mens_jewelry = Column(Integer)
    womens_accessories = Column(Integer)
    womens_clothes = Column(Integer)
    womens_shoes = Column(Integer)
    women_bags = Column(Integer)
    womens_beauty = Column(Integer)
    home = Column(Integer)
    kids_and_baby = Column(Integer)
    women_jewelry = Column(Integer)
    run_id = Column(Integer, ForeignKey(run_id_key))


class ShBrandsData(Base):
    __tablename__ = config.SH_BRANDSDATA_TABLE
    brand = Column(String(255), index=True)
    merchants = Column(Integer)
    max_products = Column(Integer)
    max_products_name = Column(String(255))
    main_category = Column(String(255))
    sub_category = Column(String(255))
    max_sale_products = Column(Integer)
    max_sale_products_name = Column(String(255))
    second_best_products = Column(Integer)
    second_best_products_name = Column(String(255))
    second_best_sale_products = Column(Integer)
    second_best_sale_products_name = Column(String(255))
    total_products = Column(Integer)
    total_products_onsale = Column(Integer)
    price_range = Column(String(255))
    avs = Column(Integer)
    run_id = Column(Integer, ForeignKey(run_id_key))


class ShPriceRange(Base):
    __tablename__ = config.SH_PRICERANGE_TABLE
    #id_row
    brand = Column(String(255), index=True)
    range_0_50 = Column(Integer)
    range_51_150 = Column(Integer)
    range_151_300 = Column(Integer)
    range_301_500 = Column(Integer)
    range_501_more = Column(Integer)
    run_id = Column(Integer, ForeignKey(run_id_key))

