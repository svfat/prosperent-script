#! /usr/bin/env python

import sqlsettings as cfg
import sys
import csv
csv.field_size_limit(sys.maxsize)
import MySQLdb
import MySQLdb.cursors
import time
import statprof
table = cfg.DB2_TABLE

def clean_values(values):
    try:
        result = [v.replace('\"','\\'+'\"') for v in list(values)]
        result = [v.replace("'","\\'") for v in result]
    except AttributeError as e:
        print row
        print '***'
        print row.values()
        raise e
    return result

def main():
    mydb = MySQLdb.connect(host=cfg.MYSQL_HOST,
                           user=cfg.MYSQL_USER,
                           passwd=cfg.MYSQL_PASS,
                           db=cfg.MYSQL_DATABASE, cursorclass=MySQLdb.cursors.DictCursor)
    cursor = mydb.cursor()
    sql = "SHOW TABLES like %s"
    cursor.execute(sql, [table])
    result = cursor.fetchone()
    if not result:
        print "Table %s not found. Creating..." % table
        cursor.execute("CREATE TABLE %s( \
                        `id` INT NOT NULL AUTO_INCREMENT, \
                        `merchant` VARCHAR(50), \
                        `category` VARCHAR(500), \
                        `description` TEXT, \
                        `keyword` VARCHAR(255), \
                        `brand` VARCHAR(100), \
                        `price_sale` VARCHAR(20), \
                        `price` VARCHAR(20), \
                        `New_Keyword` VARCHAR(255) UNIQUE, \
                        `Parsed_Keyword` VARCHAR(255), \
                        `label_1_New_Category_1` VARCHAR(30), \
                        `label_2_New_Category_2` VARCHAR(30), \
                        `label_3_New_Category_3` VARCHAR(30), \
                        `label_4_Chosen_Price` VARCHAR(20), \
                        `label_5_Chosen_Merchant` VARCHAR(50), \
                        `label_6_Merchant_Count` VARCHAR(5), \
                        `label_7_Price_Range` VARCHAR(40), \
                        `label_8_Number_of_results` VARCHAR(10), \
                        `Headline_v1` VARCHAR(100), \
                        `Headline_v2` VARCHAR(100), \
                        `Desc_v1` VARCHAR(50), \
                        `Desc_v2` VARCHAR(50), \
                        `Destination_URL` VARCHAR(255), \
                        `Display_URL` VARCHAR(100), \
                        `Adgroup_Max_CPC` VARCHAR(30), \
                        `Campaign_name` VARCHAR(100), \
                        `Adgroup` VARCHAR(100), \
                        `merchant_ID` VARCHAR(10), \
                        `error_urls_num` INTEGER, \
                        `Changed` VARCHAR(10) NOT NULL DEFAULT 'NONE', \
                        `Manual` VARCHAR(10) NOT NULL DEFAULT 'NONE', \
                        `Adgroup_status` VARCHAR(10) DEFAULT 'ENABLED', \
                        `Affiliate_urls` MEDIUMTEXT, \
                        `Chunk` INT DEFAULT NULL, \
                        `To_upload` TINYINT(1) DEFAULT 0, \
                        `url_status` VARCHAR(10) DEFAULT 'ENABLED', \
                        `Done` VARCHAR(5) NOT NULL DEFAULT 'FALSE', \
                        PRIMARY KEY (`id`) \
                         )" % table)
    else:
        print "Updating items status..."
        sql = "UPDATE `%s` SET `Changed`='NONE'" % table
        cursor.execute(sql)
        mydb.commit()
    print "Searching for manual"


    t = "CREATE INDEX keyword_index ON %s (New_Keyword)" % table
    try:
        cursor.execute(t)
    except:
        print 'Cannot create index'



    #find all 'manual' entries
    manual = set()
    sql = 'SELECT * FROM `%s` WHERE `Manual`!="NONE"' % table
    cursor.execute(sql)
    result = cursor.fetchall()
    for row in result:
        print row
        manual.add(row['New_Keyword']) #adds keyword to set
    values_query = ""
    rows_inserted = 0
    print "Updating database..."
    total_i = 0
    keys = []
    with open(cfg.DB2_FILENAME) as fn:
        data = csv.DictReader(fn, dialect='prosperent')
        for (i, row) in enumerate(data):
            if not keys:
                keys = row.keys()
            if i % 1000 == 0:
                print "%s records processed | %d new records" % (i, rows_inserted)
            if row['New_Keyword'] not in manual:
                # if not manual then search in table
                query = "SELECT * FROM `%s` WHERE `New_Keyword`=%%s" % table
                cursor.execute(query, [row['New_Keyword']])
                result = cursor.fetchone()
                if result:
                    change_flag = False
                    change_key = None
                    for key in keys:
                        if row[key] and result[key]:
                            if result[key].strip() == row[key].strip():
                                change_flag = False
                            else:
                                # data have changed
                                change_flag = True
                                change_key = key
                                break
                    if change_flag:
                        print 'found changed', change_key
                        done = result['Done']
                        to_upload = result['To_upload']

                        query = 'DELETE FROM %s WHERE `New_Keyword`=%%s' % table
                        cursor.execute(query, [row['New_Keyword']])
                        clvalues = clean_values(row.values())
                        values = '"'+'", "'.join(clvalues)+'", "CHANGED", "%s"' % to_upload
                        keysjoin = "`, `".join(keys)+'`, `Changed`, `To_upload'
                        query = "INSERT INTO %s(`%s`) VALUES(%s)" % (table, keysjoin, values)
                        cursor.execute(query)
                        rows_inserted += cursor.rowcount
                        mydb.commit()
                    else:
                        # data haven't changed
                        # doing nothing
                        pass
                else:
                    # new data

                    clvalues = clean_values(row.values())

                    values = '("%s", "NEW")' % ('", "'.join(clvalues))
                    values_query = "%s,%s" % (values_query, values)
                    if len(values_query) > 1024*1024:
                        keysjoin = '`%s`, `Changed`' % ("`, `".join(keys))
                        query = 'INSERT IGNORE INTO %s(%s) VALUES %s' % (table, keysjoin, values_query[1:])
                        cursor.execute(query)
                        rows_inserted += cursor.rowcount
                        values_query = ""
                        mydb.commit()
            else:
                print "Manual row"
            total_i = i
    if values_query:
        keysjoin = '`%s`, `Changed`' % ("`, `".join(keys))
        query = 'INSERT IGNORE INTO %s(%s) VALUES %s' % (table, keysjoin, values_query[1:])
        try:
            cursor.execute(query)
        except:
            print(cursor._last_executed)
            raise
        rows_inserted += cursor.rowcount
        values_query = ""
        mydb.commit()
    print "Total: %s records processed | %d new records" % (total_i, rows_inserted)



    #searching for disabled items
    print "Searching for disabled records..."
    query = 'SELECT `New_Keyword`, `Adgroup_status` FROM %s' % table
    cursor.execute(query)
    result = cursor.fetchall()
    db_keywords = []
    data_keywords = []
    for r in result:
        if r['Adgroup_status'] == "ENABLED":
            db_keywords.append(r['New_Keyword'])
    with open(cfg.DB2_FILENAME) as fn:
        data = csv.DictReader(fn, dialect='prosperent')
        for row in data:
            data_keywords.append(row['New_Keyword'])
    difference = set(db_keywords).difference(data_keywords)
    print "Found %d disabled records" % len(difference)
    i = 0
    for (i, d) in enumerate(list(difference)):
        if i % 1000 == 0:
            print "%d records processed" % i
        query = 'UPDATE db2 SET `label_8_Number_of_results`="0",`Changed`="CHANGED",`Adgroup_status`="PAUSED" WHERE `New_Keyword`=%s'
        try:
            cursor.execute(query, [d])
        except:
            print(cursor._last_executed)
            raise
        mydb.commit()
    print "Total: %d records processed" % i
    #close the connection to the database.

    query = 'UPDATE `db2` SET `label_8_Number_of_results`="0" WHERE `Adgroup_status`="PAUSED"'
    cursor.execute(query)


    cursor.close()


if __name__ == "__main__":
    start_time = time.time()
    main()
    print "Done"
    print "--- %s seconds ---" % (time.time() - start_time)

