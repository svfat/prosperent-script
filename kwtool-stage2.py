#! /usr/bin/env python
#coding: utf-8

"""
    kwtool.py - automatic campaign builder
    Author: Stanislav Fateev <ftumane@gmail.com>
"""
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import text
import config
import sqlsettings as cfg
import prosperent
import difflib
import string
import sys
import os
import helper
import logging
import threading
from kwtoolconfig import *
from Queue import Queue
q = Queue(cfg.THREADS*2)
logger = logging.getLogger(__name__)


keywords_dict = {}





class QueryJob(threading.Thread):
    "Job for downloading a given URL."
    def __init__(self, q, merchants, api, l):
        super(QueryJob, self).__init__()
        self.q = q
        self.l = l
        self.api = api
        self.merch_ids = '|'.join([str(m) for m in merchants])
    def run(self):
        while True:
            self._run()
    def _run(self):
        self.query = self.q.get()
        if self.merch_ids:
            result = self.api.q(query_url=cfg.API_SEARCH,
                                query=self.query,
                                sphinx_limit=1000,
                                filter_merchs=self.merch_ids,
                                sphinx_limit_pass=cfg.SPHINX_LIMIT_PASS,
                                relevancy_threshold=1,
                                page=cfg.PAGE,
                                )
        else:
            result = self.api.q(query_url=cfg.API_SEARCH,
                                query=self.query,
                                sphinx_limit=1000,
                                sphinx_limit_pass=cfg.SPHINX_LIMIT_PASS,
                                relevancy_threshold=1,
                                page=cfg.PAGE,
                                )
        if result['data']:
            keywords_dict[self.query] = len(result['data'])
        else:
            keywords_dict[self.query] = 0
        self.l.info("%s - %d" % (self.query, keywords_dict[self.query]))
        self.q.task_done()

def main():
    """ Main function """
    try:
        db_name = sys.argv[1]
    except IndexError:
        print "Usage: %s <dbname>" % sys.argv[0]
        sys.exit(1)

    logname = 'kwtool-%s.log' % db_name
    logfn = os.path.join(os.curdir, 'log', logname)
    handl = logging.FileHandler(filename = logfn, mode = 'w')
    fmt = logging.Formatter(u'%(levelname)-8s [%(asctime)s] %(message)s')
    handl.setFormatter(fmt)
    handl.setLevel(logging.INFO)
    logger.addHandler(handl)
    logger.setLevel(logging.INFO)
    lock = helper.Lock('kwtool-%s' % db_name)
    if lock.islocked():
        logger.critical('Already running. Lock file is present')
        sys.exit(1)
    lock.lock()
    logger.info("Kwtool stage 2 started")
    engine = create_engine('mysql://{0}:{1}@{2}/{3}'.format(config.MYSQL_USER,
                                                            config.MYSQL_PASS,
                                                            config.MYSQL_HOST,
                                                            db_name))

    session = sessionmaker()
    session.configure(bind=engine)
    Base.metadata.create_all(engine)
    s = session()
    api = prosperent.api(apikey=cfg.API_KEY, apiurl=cfg.API_URL)
    merchants = [value[0] for value in s.query(Merchants).all()]

    for i in xrange(cfg.THREADS):
        job = QueryJob(q=q, merchants=merchants, api=api, l=logger)
        job.setDaemon(True)
        job.start()


    kw_list = []
    for i,camp in enumerate(s.query(Campaigns).all()):
        kw_list.append(camp.keyword)

    for item in kw_list:
        q.put(item)
    q.join()

    t = text("CREATE INDEX keyword_index ON %s (keyword)" % config.KW_CAMPAIGNS_TABLE)
    try:
      s.execute(t)
    except:
      logger.error('Cannot create index')

    t = text("UPDATE %s SET number_of_results=:numb, adgroup_enabled=:enabled WHERE keyword=:key" % config.KW_CAMPAIGNS_TABLE)

    for i,key in enumerate(keywords_dict.keys()):
        numb = keywords_dict[key]
        enabled = int(bool(numb))
        s.execute(t, params=dict(numb=numb, enabled=enabled, key=key))
        if i % 1000 == 0:
            s.commit()
            logger.info(i)



    logger.info("Commiting")

    s.commit()
    logger.info("Updating adgroup_enabled")

    s.query(Campaigns)

    lock.unlock()
    logger.info("Kwtool stage 2 finished")


if __name__ == "__main__":
    main()
