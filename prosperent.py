#! /usr/bin/env python

""" Module describing Prosperent API """

import sqlsettings as cfg
import time
import statprof

import urllib3
try:
    import ujson as json
except ImportError:
    print "cannot import ujson"
    import json
import socket

import logging


class URLException(Exception):
        def __init__(self, value):
            self.value = value
        def __str__(self):
            return repr(self.value)

class TimeoutException(Exception):
        def __init__(self, value):
            self.value = value
        def __str__(self):
            return repr(self.value)

class ErrorException(Exception):
        def __init__(self, value):
            self.value = value
        def __str__(self):
            return repr(self.value)

class api(object):
    """ Describe Prosperent API """
    def __init__(self, apiurl, apikey):
        super(api, self).__init__()
        self.apiurl = apiurl
        self.apikey = apikey
        self.initpool()

    def initpool(self):
        self.pool = urllib3.HTTPConnectionPool(self.apiurl, maxsize=cfg.THREADS*2, timeout=cfg.REQUEST_TIMEOUT)

    def q(self,
          query_url,
          query=None,
          filter_merchs=None,
          filter_brand=None,
          page=None,
          sphinx_limit=None,
          sphinx_limit_pass=None,
          relevancy_threshold=None,
          filter_price=None,
          limit=None
          ):
        fields = {'api_key': self.apikey}

        if query:
            fields['query'] = query
        if filter_merchs:
            fields['filterMerchantId'] = filter_merchs
        if filter_brand:
            fields['filterBrand'] = filter_brand
        if page:
            fields['page'] = page
        if sphinx_limit:
            fields['sphinxLimit'] = sphinx_limit
        if sphinx_limit_pass:
            fields['sphinxLimitPass'] = sphinx_limit_pass
        if relevancy_threshold:
            fields['relevancyThreshold'] = relevancy_threshold
        if filter_price:
            fields['filterPrice'] = filter_price
        if limit:
            fields['limit'] = limit
            # Send the request
        try:
            handle = self.pool.request('GET', query_url, fields, timeout=cfg.REQUEST_TIMEOUT, retries=urllib3.Retry(3))
        except socket.error as e:
            logging.error("socket.error")
            return {'data':'', 'status':'error'}
        except MaxRetryError as e:
            logging.error("maxretryerror")
            return {'data':'', 'status':'error'}
        except ProtocolError as e:
            logging.error("protocolerror")
            return {'data':'', 'status':'error'}
        except KeyboardInterrupt as e:
            raise e
        except BaseException as e:
            raise e
        else:
            # Convert the json response to a dictionary
            try:
                response = json.loads(handle.data)
            except ValueError as e:
                return {'data':'', 'status':'error'}
            # Check for errors
            if response['errors']:
                raise ErrorException (response['errors'])
            # Set specified data from response
            data = response['data']
            return {'data':data, 'status':'ok'}



def main():
    # just test
    a = api(apiurl=cfg.API_URL, apikey=cfg.API_KEY)
    print a.q('scully scully sparkles and guitars shirt (charcoal)long sleeve button up')
    pass

if __name__=="__main__":

    if cfg.DEBUG:
        start_time = time.time()
        statprof.start()
        try:
            main()
        finally:
            statprof.stop()
            statprof.display()
            print("--- %s seconds ---" % (time.time() - start_time))
    else:
        main()

