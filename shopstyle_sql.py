bigsql = ["""set profiling=1;""","""DROP PROCEDURE IF EXISTS DOROW;""",
            """CREATE PROCEDURE DOROW(IN brandname VARCHAR(255), runid INT)
BEGIN
    DROP TABLE IF EXISTS t, category, category_tmp, pr;
    CREATE TABLE t ENGINE=MEMORY AS (SELECT * FROM `brands_merchants` WHERE `brand`=brandname AND `run_id`=runid);
    CREATE TABLE category ENGINE=MEMORY  AS (SELECT * FROM `brandscategories` WHERE `brand`=brandname AND `run_id`=runid);
    CREATE TABLE category_tmp ENGINE=MEMORY AS
        SELECT `mens_accessories` as amt,'mens_accessories' as name from `category` union
        select `mens_clothes`,'mens_clothes' from `category` union
        select `mens_shoes`,'mens_shoes' from `category` union
        select `mens_bags`,'mens_bags' from `category` union
        select `mens_grooming`,'mens_grooming' from `category` union
        select `mens_jewelry`,'mens_jewelry' from `category` union
        select `womens_accessories`,'womens_accessories' from `category` union
        select `womens_clothes`,'womens_clothes' from `category` union
        select `womens_shoes`,'womens_shoes' from `category` union
        select `women_bags`,'women_bags' from `category` union
        select `womens_beauty`,'womens_beauty' from `category` union
        select `home`,'home' from `category` union
        select `kids_and_baby`,'kids_and_baby' from `category` union
        select `women_jewelry`,'women_jewelry' from `category`;
    CREATE TEMPORARY TABLE `pr` ENGINE=MEMORY AS SELECT CASE GREATEST(`range_0_50`,`range_51_150`,`range_151_300`,`range_301_500`,`range_501_more`)
            WHEN `range_0_50` THEN '0-50'
            WHEN `range_51_150` THEN '51-150'
            WHEN `range_151_300` THEN '151-300'
            WHEN `range_301_500` THEN '301-500'
            WHEN `range_501_more` THEN '501+'
            END AS `amt`
    FROM `pricerange`  WHERE `brand`=brandname AND `run_id`=runid;
    INSERT INTO `brandsdata`
    SELECT
            null,
            brandname,
            count(t.`merchant`) AS `merchants`,
            (SELECT `value` FROM `t` ORDER BY `value` DESC LIMIT 0,1) AS `max_products`,
            (SELECT `merchant` FROM `t` ORDER BY `value` DESC LIMIT 0,1) AS `max_products_name`,
            (SELECT `name` FROM `category_tmp` ORDER BY `amt` DESC LIMIT 0,1) AS `main_category`,
            (SELECT IFNULL((SELECT `name` FROM `category_tmp` ORDER BY `amt` DESC LIMIT 1,1),
                           (SELECT `name` FROM `category_tmp` ORDER BY `amt` DESC LIMIT 0,1)))
                            AS `sub_category`,
            (SELECT `value_onsale` FROM `t`  ORDER BY `value_onsale` DESC LIMIT 0,1)
                            AS `max_sale_products`,
            (SELECT `merchant` FROM `t`  ORDER BY `value_onsale` DESC LIMIT 0,1)
                            AS `max_sale_products_name`,
            (SELECT IFNULL((SELECT `value` FROM `t` ORDER BY `value_onsale` DESC LIMIT 1,1),
                          (SELECT `value` FROM `t` ORDER BY `value_onsale` DESC LIMIT 0,1)))
                           AS `second_best_products`,
            (SELECT IFNULL((SELECT `merchant` FROM `t` ORDER BY `value_onsale` DESC LIMIT 1,1),
                          (SELECT `merchant` FROM `t` ORDER BY `value_onsale` DESC LIMIT 0,1)))
                          AS `second_best_products_name`,
            (SELECT IFNULL((SELECT `value_onsale` FROM `t`  ORDER BY `value_onsale` DESC LIMIT 1,1),
                          (SELECT `value_onsale` FROM `t`  ORDER BY `value_onsale` DESC LIMIT 0,1)))
                            AS `second_best_sale_products`,
            (SELECT IFNULL((SELECT `merchant` FROM `t`  ORDER BY `value_onsale` DESC LIMIT 1,1),
                          (SELECT `merchant` FROM `t`  ORDER BY `value_onsale` DESC LIMIT 0,1)))
                          AS `second_best_sale_products_name`,
            (SELECT SUM(`value`) FROM `t`) AS `total_products`,
            (SELECT SUM(`value_onsale`) FROM `t`) AS `total_products_onsale`,
            pr.`amt` AS `price_range`,
            (SELECT SUM(`search_volume`) FROM kw_aw_campaigns_db.keywords WHERE `brand_id` =
                (SELECT `id` FROM kw_aw_campaigns_db.brands WHERE `brand_name`=brandname)) AS `ave_brand_searches`,
            runid
    FROM t, pr;
    DROP TABLE IF EXISTS t, category, category_tmp, pr;
END;"""]


