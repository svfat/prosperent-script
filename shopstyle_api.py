#! /usr/bin/env python

""" Module describing Shopstyle API """

import time
import statprof

import urllib3
try:
    import ujson as json
except ImportError:
    print "cannot import ujson"
    import json

import logging
logging.basicConfig(level=logging.INFO)

class api(object):
    """ Describe Prosperent API """
    def __init__(self, apiurl, maxsize=30, timeout=1, retries=3):
        super(api, self).__init__()
        self.apiurl = apiurl
        self.initpool(maxsize, timeout)
        self.timeout = timeout
        self.retries = retries

    def initpool(self, maxsize, timeout):
        self.pool = urllib3.HTTPConnectionPool(self.apiurl, maxsize=maxsize, timeout=timeout)

    def q(self, endpoint, **kwargs):
        fields = kwargs
        try:
            handle = self.pool.request('GET', endpoint, fields, timeout=self.timeout, retries=urllib3.Retry(self.retries))
        except KeyboardInterrupt as e:
            raise e
        except BaseException as e:
            print "Something went wrong..."
            print fields
            raise e
        else:
            # Convert the json response to a dictionary
            try:
                response = json.loads(handle.data)
            except ValueError as e:
                return None
            return response


def main():
    pass

if __name__=="__main__":

    if cfg.DEBUG:
        start_time = time.time()
        statprof.start()
        try:
            main()
        finally:
            statprof.stop()
            statprof.display()
            print("--- %s seconds ---" % (time.time() - start_time))
    else:
        main()

