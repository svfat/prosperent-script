#!/bin/sh
source env/bin/activate
./callapi.py
echo "python callapi.py" >> log.txt
./convert.py
echo "python convert.py" >> log.txt
./merge_db1.py
echo "python merge_db1.py" >> log.txt
./merge_db2.py
echo "python merge_db2.py" >> log.txt
./populate_db1.py
echo "python populate_db1.py" >> log.txt
./populate_db2.py
echo "python populate_db2.py" >> log.txt
