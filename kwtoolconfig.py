import config
from sqlalchemy import create_engine, Column, Integer, String, Boolean

from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Brands(Base):
    """ SQLAlchemy class """
    __tablename__ = config.KW_BRANDS_TABLE
    __table_args__ = {'mysql_charset': 'utf8', 'mysql_engine': 'InnoDB'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    brand_name = Column(String(250), nullable=False)

class WordsToRemove(Base):
    """ SQLAlchemy class """
    __tablename__ = config.KW_WORDSTOREMOVE_TABLE
    __table_args__ = {'mysql_charset': 'utf8', 'mysql_engine': 'InnoDB'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    word = Column(String(250), nullable=False)

class PrefWords(Base):
    """ SQLAlchemy class """
    __tablename__ = config.KW_PREFWORDS_TABLE
    __table_args__ = {'mysql_charset': 'utf8', 'mysql_engine': 'InnoDB'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    word = Column(String(250), nullable=False)

class OmitWords(Base):
    """ SQLAlchemy class """
    __tablename__ = config.KW_OMITWORDS_TABLE
    __table_args__ = {'mysql_charset': 'utf8', 'mysql_engine': 'InnoDB'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    word = Column(String(250), nullable=False)

class WordsFirst(Base):
    __tablename__ = config.KW_WORDSFIRST_TABLE
    __table_args__ = {'mysql_charset': 'utf8', 'mysql_engine': 'InnoDB'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    word = Column(String(250), nullable=False)

class WordsLast(Base):
    __tablename__ = config.KW_WORDSLAST_TABLE
    __table_args__ = {'mysql_charset': 'utf8', 'mysql_engine': 'InnoDB'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    word = Column(String(250), nullable=False)


class WordsToReplace(Base):
    """ SQLAlchemy class """
    __tablename__ = config.KW_WORDSTOREPLACE_TABLE
    __table_args__ = {'mysql_charset': 'utf8', 'mysql_engine': 'InnoDB'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    a = Column(String(250), nullable=False)
    b = Column(String(250), nullable=False)

class ConfigTable(Base):
    """ SQLAlchemy class """
    __tablename__ = config.KW_CONFIG_TABLE
    __table_args__ = {'mysql_charset': 'utf8', 'mysql_engine': 'InnoDB'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(250), nullable=False)
    value = Column(String(250), nullable=False)


class WordsWebsite(Base):
    """ SQLAlchemy class """
    __tablename__ = config.KW_WORDSWEBSITE_TABLE
    __table_args__ = {'mysql_charset': 'utf8', 'mysql_engine': 'InnoDB'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    word = Column(String(250), nullable=False)


class Keywords(Base):
    """ SQLAlchemy class """
    __tablename__ = config.KW_KEYWORDS_TABLE
    __table_args__ = {'mysql_charset': 'utf8', 'mysql_engine': 'InnoDB'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    brand_id = Column(Integer)
    keyword = Column(String(250), nullable=False)
    search_volume = Column(Integer)

class Campaigns(Base):
    """ SQLAlchemy class """
    __tablename__ = config.KW_CAMPAIGNS_TABLE

    id = Column(Integer, primary_key=True, autoincrement=True)
    brand_id = Column(Integer)
    keyword = Column(String(250), nullable=False, index=True, unique=True)
    adgroup = Column(String(250), nullable=False)
    headline1 = Column(String(250))
    headline2 = Column(String(250))
    descline1 = Column(String(250))
    descline2 = Column(String(250))
    displayurl = Column(String(250))
    desturl = Column(String(250))
    campaign = Column(String(250), nullable=False)
    label_1 = Column(String(250))
    to_upload = Column(Boolean, default=False)
    done = Column(Boolean, default=False)
    adgroup_enabled = Column(Boolean, default=True)
    number_of_results = Column(Integer)
    number_of_searches = Column(Integer)


class Merchants(Base):
    __tablename__ = config.KW_MERCHANTS_TABLE
    __table_args__ = {'mysql_charset': 'utf8', 'mysql_engine': 'InnoDB'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    prosperent_id = Column(Integer)
    name = Column(String(250), nullable=False)

class AdFormats(Base):
    """ SQLAlchemy class """
    __tablename__ = config.KW_ADFORMATS_TABLE
    id = Column(Integer, primary_key=True, autoincrement=True)
    headline_v1  = Column(String(250))
    headline_v2  = Column(String(250))
    desc_v1  = Column(String(250))
    desc_v2  = Column(String(250))
    displayurl  = Column(String(250))
    desturl  = Column(String(250))

