#! /usr/bin/env python

import sys
import csv
csv.field_size_limit(sys.maxsize)
import time
import sqlsettings as cfg
import os
import shutil
import prosperent
import logging
import threading
from Queue import Queue
import helper

queries = {}
request_number = 0
start_time = time.time()
error_list = []
dupes_keywords = set()
brands_to_remove = set(helper.return_column_sql(cfg.BRANDS_TO_REMOVE_TABLE, 'brand_name'))
q = Queue(cfg.THREADS*2)

class QueryJob(threading.Thread):
    "Job for downloading a given URL."
    def __init__(self, i, q, merchants, api):
        super(QueryJob, self).__init__()
        self.i = i
        self.q = q
        self.api = api
        self.merch_ids = '|'.join([str(m) for m in merchants])
    def run(self):
        while True:
            self._run()
    def _run(self):
        self.query = self.q.get()
        global queries, request_number
        #print "api call: %s" % self.query

        if self.merch_ids:
            result = self.api.q(query_url=cfg.API_SEARCH,
                                query=self.query,
                                sphinx_limit=1000,
                                filter_merchs=self.merch_ids,
                                sphinx_limit_pass=cfg.SPHINX_LIMIT_PASS,
                                relevancy_threshold=cfg.RELEVANCY_THRESHOLD,
                                page=cfg.PAGE,
                                )
        else:
            result = self.api.q(query_url=cfg.API_SEARCH,
                                query=self.query,
                                sphinx_limit=1000,
                                sphinx_limit_pass=cfg.SPHINX_LIMIT_PASS,
                                relevancy_threshold=cfg.RELEVANCY_THRESHOLD,
                                page=cfg.PAGE,
                                )
        request_number += 1
        queries[self.query] = {}
        affiliate_urls = set()
        if result['data']:
            if cfg.REMOVE_DUP_SWITCH:
                tmp_result = result['data'][:]
                desc_dup = set()
                for r in tmp_result:
                    if r['description']: # check if description isn't empty
                        if r['description'] in desc_dup:
                            for i, l in enumerate(result['data']):
                                if l['description'] == r['description']:
                                    del result['data'][i]
                                    break
                        else:
                            desc_dup.add(r['description'])
            price_set = set()
            merch_set = set()
            for r in result['data']:
                try:
                    price_set.add(float(r['price']))
                except ValueError:
                    pass
                try:
                    price_set.add(float(r['price_sale']))
                except ValueError:
                    pass
                merch_set.add(r['merchant'])
                r['affiliate_url'] = r['affiliate_url'].replace("http://prosperent.com/store/product/", "")
                affiliate_urls.add(r['affiliate_url'])
            number_results = len(result['data'])
            number_merchs = len(merch_set)
            if number_merchs == 1 and number_results == 1:
                result_price = min(price_set)
                result_merchant = merch_set.pop()
                merchant_count = 1
            elif number_merchs >= 1 and number_results > 1:
                if len(price_set) == 1:
                    result_price = price_set.pop()
                else:
                    result_price = second_smallest(price_set)
                result_merchant = ''
                merchant_count = number_merchs
            elif len(merch_set) == 0:
                result_price = 0
                result_merchant = ''
                merchant_count = 0
            aff_str = ''
            for url in list(affiliate_urls):
                aff_str += url.replace(",","$comma$")+'|||'
            queries[self.query]['affiliate_urls'] = aff_str
            queries[self.query]['number_of_results'] = number_results
            queries[self.query]['chosen_price'] = str(result_price)
            queries[self.query]['chosen_merchant'] = result_merchant.encode('utf-8')
            queries[self.query]['merchant_count'] = str(merchant_count)
            if not merchant_count:
                queries[self.query]['adgroup_status'] = "PAUSED"
                queries[self.query]['number_of_results'] = 0
            else:
                queries[self.query]['adgroup_status'] = "ENABLED"
        else:
            queries[self.query]['affiliate_urls'] = ''
            queries[self.query]['number_of_results'] = 0
            queries[self.query]['chosen_price'] = ''
            queries[self.query]['chosen_merchant'] = ''
            queries[self.query]['merchant_count'] = ''
            queries[self.query]['adgroup_status'] = "PAUSED"
        if result['status'] == 'error':
            error_list.append(self.query)
            print 'error'
        work_time = time.time() - start_time
        speed = '\r%s requests per second                    ' % int(request_number / work_time)
        print speed,
        self.q.task_done()



def get_merch_id(merchant, dictionary):
    return dictionary[merchant]


def get_brand_replace(brand, dictionary):
    try:
        return dictionary[brand]
    except KeyError:
        return None

def second_smallest(numbers):
    m1, m2 = float('inf'), float('inf')
    for x in numbers:
        if x <= m1:
            m1, m2 = x, m1
        elif x < m2:
            m2 = x
    return m2

def categories_search(categories_dict, keyword, category):

    def search_in_dict(categories_dict, string):
        def word_in_str(word,string):
            word = word.lower()
            return ((' %s ' % word) in string) or \
               ((' %s,' % word) in string) or \
               ((')%s ' % word) in string)
        for r in categories_dict:
            if r['our_category_3']:
                if word_in_str(r['our_category_3'], string):
                    return (r['our_category_1'],
                            r['our_category_2'],
                            r['our_category_3'])
        for r in categories_dict:
            if r['our_category_2']:
                if word_in_str(r['our_category_2'], string):
                    return (r['our_category_1'],
                            r['our_category_2'],
                            "")
        for r in categories_dict:
            if r['our_category_1']:
                if word_in_str(r['our_category_1'], string):
                    return (r['our_category_1'],
                            "",
                            "")

        return "", "", ""

    a, b, c = search_in_dict(categories_dict, keyword)
    if a:
        return a, b, c
    else:
        a, b, c = search_in_dict(categories_dict, category)
        if a:
            return a, b, c
        else:
            #a,b,c = search_in_dict(categories_dict, description)
            return a, b, c

def _stage_1(filename, merchants_dict, brands_to_replace):
    with open(filename, "rb") as source:
        rdr = csv.reader(source, dialect='prosperent')
        output = []

        rdr.next()

        print "1st stage"
        for (j, r) in enumerate(rdr):

            if (j % 10000 == 0) and (j != 0):
                print "%s records" % j

            n = {}
            #delete wrong items from output
            try:
                float(r[6])
            except:
                try:
                    print '''Wrong value 'price' at %s''' % r[3].lower().strip().decode('unicode_escape').encode('ascii','ignore')
                except IndexError as e:
                    print e
                continue

            keyword_removed = " ".join(r[3].split()).lower().strip().decode('unicode_escape').encode('ascii','ignore')
            for keyword in cfg.REMOVE_FROM_KEYWORD_BEFORE_PARSING:
                if keyword in keyword_removed:
                    keyword_removed = keyword_removed.replace(keyword," ")


            n['original_keyword'] = " ".join(r[3].split()).lower().strip()
            n['keyword'] = " ".join(keyword_removed.split()).strip()
            n['description'] = " ".join(r[4].split()).lower().strip().decode('unicode_escape').encode('ascii','ignore')
            n['category'] = " ".join(r[5].split()).lower().strip().decode('unicode_escape').encode('ascii','ignore')
            n['price'] = " ".join(r[6].split()).lower().strip().decode('unicode_escape').encode('ascii','ignore')
            n['price sale'] = " ".join(r[7].split()).lower().strip().decode('unicode_escape').encode('ascii','ignore')
            n['merchant'] = " ".join(r[1].split()).lower().strip().decode('unicode_escape').encode('ascii','ignore')

            try:
                n['merchant ID'] = get_merch_id(n['merchant'], merchants_dict)
            except KeyError:
                print 'Cannot find %s in %s' % (n['merchant'], cfg.MERCHS_TABLE)
                continue

            n['original_brand'] = " ".join(r[0].split()).lower().strip().decode('unicode_escape').encode('ascii','ignore')
            n['parsed_brand'] = helper.replace_words(n['original_brand'])
            keyword_removed = n['parsed_brand']
            for keyword in cfg.REMOVE_FROM_BRAND:
                keyword_removed = keyword_removed.replace(keyword, "")
            n['parsed_brand'] = " ".join(keyword_removed.split())
            replaced = get_brand_replace(n['parsed_brand'], brands_to_replace)
            if replaced:
                n['parsed_brand'] = replaced

            if n['parsed_brand'] in brands_to_remove:
                continue
            tmp = n['keyword'].replace(n['original_brand'], "").strip()
            tmp = tmp.replace(n['parsed_brand'], "").strip()
            for rule in cfg.PARSER_RULES:
                if str(rule['merchant_id']) == str(n['merchant ID']):
                    rules_list = [(rule['1'],rule['between_1']),
                                  (rule['2'],rule['between_2']),
                                  (rule['3'],rule['between_3'])]
                    for r in rules_list:
                        s = r[0]
                        betw = r[1]
                        if s and (s in tmp):
                            if s == '\"':
                                if betw and (' \"' in tmp):
                                    tmp = tmp.split(' \"')[1]
                                    tmp = tmp.split('\"')[0]
                                    break
                            elif betw and (tmp.count(s) > 1):
                                tmp = tmp.split(s)[1]
                                break
                            else:
                                if r == '\'':
                                    tmp = tmp.split(s)[1]
                                    break
                                else:
                                    tmp = tmp.split(s)[0]
                                    break
                    n['Parsed Keyword'] = " ".join(tmp.split())
                    break
            try:
                n['Parsed Keyword']
            except KeyError:
                n['Parsed Keyword'] = " ".join(tmp.split())
                print "Cannot find rule for merchant %s to parse keyword" % n['merchant']


            # parse adgroup
            keyword_removed = n['Parsed Keyword']
            for keyword in cfg.REMOVE_FROM_PARSED_KEYWORD:
                keyword_removed = keyword_removed.replace(keyword, "")

            n['Parsed Keyword'] = keyword_removed
            n['Adgroup'] = " ".join(keyword_removed.split()[:cfg.WORDS_COUNT])



            # parse new keyword
            n['New Keyword'] = n['parsed_brand']+" "+" ".join(keyword_removed.split()[:cfg.WORDS_COUNT])

            if n['New Keyword'] not in dupes_keywords: # next N if we have same item
                dupes_keywords.add(n['New Keyword'])
            else:
                continue

            # convert brand
            n['Campaign name'] = n['parsed_brand'].title()
            for letter in n['Campaign name'][:]:
                if (letter == '\'') and (n['Campaign name'].index(letter)+1 != len(n['Campaign name'])):
                    list1 = list(n['Campaign name'])
                    list1[n['Campaign name'].index(letter)+1] = list1[n['Campaign name'].index(letter)+1].lower()
                    n['Campaign name'] = ''.join(list1)
            # write parsed data to output
            output.append(n)
    return output

def _stage_2(output, api, merchants_query):
    def _dojob(querieslist, api):
        dupes = set()
        for n in querieslist:
            try:
                if n['New Keyword'] in dupes: # next N if we have same item
                    continue
                dupes.add(n['New Keyword'])
                q.put(n['New Keyword'])
            except TypeError:
                raise
        q.join()
        return

    global error_list, queries
    queries = {}
    print "2nd stage - %d records to go" % len(output)

    if cfg.REMOVE_DUP_SWITCH:
        print("REMOVE_DUP_SWITCH = %s, will remove duplicates from queries" % cfg.REMOVE_DUP_SWITCH)
    else:
        print("REMOVE_DUP_SWITCH = %s, will not remove duplicates from queries" % cfg.REMOVE_DUP_SWITCH)
    outp = output[:]
    _dojob(outp, api)
    while error_list: #working with errors
        print 'errors'
        api.initpool() #re-init api pool
        error_queries = sorted(error_list[:])
        error_list = []
        _dojob(error_queries, api)
    del outp
    return output

def _stage_3(output, categories_dict):
    #max cpc
    print "3rd stage - %d records to go" % len(output)
    for (j, n) in enumerate(output):
        if (j % 1000 == 0) and (j != 0):
            print "%s records" % j

          # categories search
        lskw = (' %s ' % n['keyword']).replace(","," ").replace("."," ").replace(")"," ").replace("("," ").replace(">"," ").replace("<"," ")
        lscat = (' %s ' % n['category']).replace(","," ").replace("."," ").replace(")"," ").replace("("," ").replace(">"," ").replace("<"," ")
        lsdesc = (' %s ' % n['description']).replace(","," ").replace("."," ").replace(")"," ").replace("("," ").replace(">"," ").replace("<"," ")
        a, b, c = categories_search(categories_dict, lskw, lscat)
        n['label 1 - New Category 1'] = a
        n['label 2 - New Category 2'] = b
        n['label 3 - New Category 3'] = c
        item = queries[n['New Keyword']]
        if item['adgroup_status'] != 'PAUSED':
            try:
                n['label 4 - Chosen Price'] = item['chosen_price']
                n['label 5 - Chosen Merchant'] = item['chosen_merchant']
                n['label 6 - Merchant Count'] = item['merchant_count']
                n['Adgroup status'] = 'ENABLED'
                n['Number of results'] = item['number_of_results']
                n['Affiliate urls'] = item['affiliate_urls']
            except KeyError as excp:
                print queries.keys()
                raise excp

            adgrmaxcpc = 1
            price = float(n['label 4 - Chosen Price'])
            for rule in cfg.MAX_CPC_RULES:
                rmin = float(rule['min'])
                rmax = float(rule['max'])
                rval = float(rule['value'])
                if (rmin <= price) and (price < rmax):
                    adgrmaxcpc = rval
                    for r in categories_dict:
                        if n['label 1 - New Category 1'] == r['our_category_1']:
                            adgrmaxcpc = adgrmaxcpc*float(r['multiplier_1'])
                            if not adgrmaxcpc:
                                adgrmaxcpc = rval
                        if n['label 2 - New Category 2'] == r['our_category_2']:
                            adgrmaxcpc = adgrmaxcpc*float(r['multiplier_2'])
                            if not adgrmaxcpc:
                                adgrmaxcpc = rval
                        if n['label 3 - New Category 3'] == r['our_category_3']:
                            adgrmaxcpc = adgrmaxcpc*float(r['multiplier_3'])
                            if not adgrmaxcpc:
                                adgrmaxcpc = rval
                            break
                    break

            for rule in cfg.MERCHANTS_MULTIPLIERS:
                rc = float(rule['count'])
                rm = float(rule['multiplier'])
                if int(n['label 6 - Merchant Count']) == rc:
                    adgrmaxcpc = adgrmaxcpc * rm


            n['Adgroup Max CPC'] = adgrmaxcpc

            for rule in cfg.PRICE_RANGE_RULES:
                if (rule['min'] <= price) and (price <= rule['max']):
                    n['label 7 - Price Range'] = rule['value']

            try:
                n['label 7 - Price Range']
            except KeyError:
                print "*%s*" % str(price)
                print n['New Keyword']
                raise KeyError
        else:
            n['Adgroup Max CPC'] = ''
            n['label 4 - Chosen Price'] = ''
            n['label 5 - Chosen Merchant'] = ''
            n['label 6 - Merchant Count'] = ''
            n['label 7 - Price Range'] = ''
            n['Adgroup status'] = 'PAUSED'
            n['Number of results'] = '0'
            n['Affiliate urls'] = ''

        # macros excel 2 functions
        for item in cfg.HEADLINE_V1_TEXT:
            tmp = item.replace('$brand$', n['parsed_brand'])
            if len(tmp) > cfg.HEADLINE_V1_NUMBER:
                continue
            else:
                n['Headline v1'] = tmp
                break
        for item in cfg.HEADLINE_V2_TEXT:
            cfg.KEYWORD_MAX_LEN = cfg.HEADLINE_V2_NUMBER - len(item.lower().replace('{keyword:$newkeyword*$}', ""))
            key_words = n['New Keyword'].split(' ')
            good = False
            while not good:
                tmp = item.replace('$newkeyword*$', ' '.join(key_words).title())
                if len(' '.join(key_words)) > cfg.KEYWORD_MAX_LEN:
                    try:
                        key_words.pop()
                    except IndexError:
                        good = True
                else:
                    good = True
            n['Headline v2'] = tmp
            break
        for item in cfg.DESC_V1_TEXT:
            n['Desc v1'] = item[:cfg.DESC_V1_NUMBER]
            break
        for item in cfg.DESC_V2_TEXT:
            cfg.KEYWORD_DESC_V2_MAX_LEN = cfg.DESC_V2_NUMBER - len(item.replace('$newkeyword*$', ""))
            key_words = n['New Keyword'].split(' ')
            good = False
            while not good:
                tmp = item.replace('$newkeyword*$', ' '.join(key_words))
                if len(' '.join(key_words)) > cfg.KEYWORD_DESC_V2_MAX_LEN:
                    try:
                        key_words.pop()
                    except IndexError:
                        good = True
                else:
                    good = True
            n['Desc v2'] = tmp
            break
        for item in cfg.DISPLAY_URL_TEXT:
            tmp = item.replace('$brandnospaces$', n['Campaign name'].replace(" ","").replace(".","").replace("\'",""))
            if len(tmp) > cfg.DISPLAY_URL_NUMBER:
                continue
            else:
                n['Display URL'] = tmp.lower()
                break
        n['Destination URL'] = cfg.DESTINATION_URL_TEXT[0].replace('$keyword$', n['New Keyword'].replace(' ', '%20'))

        try:
            n['Headline v1']
        except KeyError:
            n['Headline v1'] = n['parsed_brand']
        try:
            n['Headline v2']
        except KeyError:
            n['Headline v2'] = ""
        try:
            n['Desc v2']
        except KeyError:
            n['Desc v2'] = ""
        try:
            n['Display URL']
        except KeyError:
            n['Display URL'] = 'www.best-fashion-deals.com'

        n['Display URL'] = n['Display URL'].replace('..', '.')

        n['Headline v1'] = n['Headline v1'].title()
        #n['Headline v2'] = n['Headline v2'].title()#.replace('{K','{k')
        n['Headline v1'] = " ".join(n['Headline v1'].split()).strip()
        n['Headline v2'] = " ".join(n['Headline v2'].split()).strip()
        n['Desc v1'] = " ".join(n['Desc v1'].title().split()).strip()
        n['Desc v2'] = " ".join(n['Desc v2'].title().split()).strip()
    return output

def _write_to_file(filename, output):
    """ writes output to file """
    with open(filename, "wb") as result:
        wtr = csv.DictWriter(result, cfg.FIELDNAMES, dialect='prosperent')
        wtr.writeheader()
        for n in output:
            wtr.writerow(
                {'keyword':n['original_keyword'],
                 'description':n['description'],
                 'category':n['category'],
                 'price':n['price'],
                 'price_sale':n['price sale'],
                 'merchant':n['merchant'],
                 'merchant_ID':n['merchant ID'],
                 'brand':n['parsed_brand'],
                 'Campaign_name':n['Campaign name'],
                 'Adgroup':n['Adgroup'],
                 'Parsed_Keyword':n['Parsed Keyword'].strip(),
                 'New_Keyword':n['New Keyword'].strip(),
                 'label_1_New_Category_1':n['label 1 - New Category 1'],
                 'label_2_New_Category_2':n['label 2 - New Category 2'],
                 'label_3_New_Category_3':n['label 3 - New Category 3'],
                 'label_4_Chosen_Price':n['label 4 - Chosen Price'],
                 'label_5_Chosen_Merchant':n['label 5 - Chosen Merchant'],
                 'Adgroup_Max_CPC':n['Adgroup Max CPC'],
                 'label_6_Merchant_Count':n['label 6 - Merchant Count'],
                 'label_7_Price_Range':n['label 7 - Price Range'],
                 'Headline_v1':n['Headline v1'],
                 'Headline_v2':n['Headline v2'],
                 'Desc_v1':n['Desc v1'],
                 'Desc_v2':n['Desc v2'],
                 'Display_URL':n['Display URL'],
                 'Destination_URL':n['Destination URL'],
                 'Adgroup_status':n['Adgroup status'],
                 'label_8_Number_of_results':n['Number of results'],
                 'Affiliate_urls':n['Affiliate urls']})


class Converter:
    def __init__(self, categories, merchants, merchants_query, brands_to_replace):
        self.api = prosperent.api(apikey=cfg.API_KEY, apiurl=cfg.API_URL)
        self.categories = categories
        self.merchants = merchants
        self.merchants_query = merchants_query
        self.brands_to_replace = brands_to_replace
        for i in xrange(cfg.THREADS):
            job = QueryJob(i=i, q=q, merchants=merchants_query, api=self.api)
            job.setDaemon(True)
            job.start()


    def convert(self, bn):
        self.in_fn = os.path.join(cfg.OUT_DIR, bn+'.csv')
        if not os.path.isfile(self.in_fn):
            logging.ERROR("file not found %s" % self.in_fn)
            raise IOError
        output = _stage_1(self.in_fn, self.merchants, self.brands_to_replace)
        output = _stage_2(output, self.api, self.merchants_query)
        result = _stage_3(output, self.categories)
        return result

def main():
    logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s', level=logging.DEBUG, filename=u'convert.log')
    try:
        bn = sys.argv[1]
    except IndexError:
        print "Usage: %s <brandname>" % sys.argv[0]
        sys.exit(1)
    out_fn = os.path.join(cfg.OUT_DIR_DB2, bn+'.csv')
    if os.path.isfile(out_fn):
        print "file is already exists: %s" % out_fn
        raise IOError
    categories = helper.return_dict_sql(cfg.CATEGORY_TABLE)
    tmp_merchants = helper.return_dict_sql(cfg.MERCHS_TABLE)
    merchants = {}
    for d in tmp_merchants:
        merchants[d['name'].lower()] = d['prosperent_id']
    merchants_query = helper.return_column_sql(cfg.MERCHS_TABLE_2, 'prosperent_id')
    brands_to_replace_tmp_dict = helper.return_dict_sql(cfg.BRANDS_TO_REPLACE_TABLE)
    brands_to_replace = {}
    for r in brands_to_replace_tmp_dict:
        brands_to_replace[r['brand_name'].lower()] = r['replace_with'].lower()
    converter = Converter(categories, merchants, merchants_query, brands_to_replace)
    output = converter.convert(bn)
    _write_to_file(out_fn, output)


if __name__ == "__main__":
    main()
