#! /usr/bin/env python

import sqlsettings as cfg
import csv
import MySQLdb
import MySQLdb.cursors
import statprof
import time
import logging

table = cfg.DB1_TABLE

def main():
    mydb = MySQLdb.connect(host=cfg.MYSQL_HOST,
        user=cfg.MYSQL_USER,
        passwd=cfg.MYSQL_PASS,
        db=cfg.MYSQL_DATABASE, cursorclass=MySQLdb.cursors.DictCursor)
    cursor = mydb.cursor()

    cursor.execute("DROP TABLE IF EXISTS %s" % table)
    cursor.execute("CREATE TABLE %s( \
                     merchant VARCHAR(255), \
                     category VARCHAR(255), \
                     description TEXT, \
                     keyword VARCHAR(255), \
                     brand VARCHAR(255), \
                     price_sale VARCHAR(50), \
                     price VARCHAR(50), \
                     productId VARCHAR(50) \
                     )" % table)

    keys = []
    values_query = ""
    rows_inserted = 0
    with open(cfg.DB1_FILENAME, "r") as csvfile:
        csv_data = csv.DictReader(csvfile, dialect='prosperent')
        for row in csv_data:
            if not keys:
                keys = row.keys()
            tmp_values = [str(v).replace("\"", '\\"') for v in row.values()]
            try:
                float(tmp_values[6])
            except ValueError:
                continue
            values = '("%s","%s","%s","%s","%s","%s","%s","%s")' % (tmp_values[0],
                                                                    tmp_values[1],
                                                                    tmp_values[2],
                                                                    tmp_values[3],
                                                                    tmp_values[4],
                                                                    tmp_values[5],
                                                                    tmp_values[6],
                                                                    tmp_values[7],
                                                                           )
            values_query = "%s,%s" % (values_query, values)
            if len(values_query) > 1024*1024: #10kb
                query = 'INSERT INTO %s(%s) VALUES %s' % (table, ", ".join(keys), values_query[1:])
                try:
                    cursor.execute(query)
                except:
                    logging.critical(cursor._last_executed)
                    raise
                rows_inserted += cursor.rowcount
                logging.info("%d rows inserted" % rows_inserted)
                values_query = ""
                mydb.commit()
        if values_query:
            query = 'INSERT INTO %s(%s) VALUES %s' % (table, ", ".join(keys), values_query[1:])
            try:
                cursor.execute(query)
            except:
                logging.critical(cursor._last_executed)
                raise
            rows_inserted += cursor.rowcount
            logging.info("%d rows inserted" % rows_inserted)
            values_query = ""
            mydb.commit()

    #close the connection to the database.

    cursor.close()
    

if __name__ == "__main__":
    start_time = time.time()
    if cfg.DEBUG:
        main()
    else:
        main()
    print "--- %s seconds ---" % (time.time() - start_time)
