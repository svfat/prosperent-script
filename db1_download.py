#! /usr/bin/env python

"""
    Download items from Prosperent API and saves they into OUT_DIR in csv format
    Usage: python db1_download.py
    Config in config.py or local_config.py

"""

import sqlsettings as cfg
import os
import time
import re
import helper
import prosperent
import threading
from Queue import Queue
import csv
import logging
logger = logging.getLogger(__name__)
import sys

start_time = time.time()

def write_to_file(data, brand, brands_to_remove):
    fn = os.path.join(cfg.OUT_DIR, brand + '.csv')
    if os.path.isfile(fn):
        write_header = False
    else:
        write_header = True
    with open(fn, 'a') as outfile:
        wtr = csv.writer(outfile, dialect='prosperent')
        line = ('brand', 'merchant',
                'productId', 'keyword',
                'description', 'category',
                'price', 'price_sale')
        if write_header:
            wtr.writerow(line) #header
        for row in data:
            try:
                if row['brand'].strip().lower() not in brands_to_remove:
                    wtr.writerow((helper.r_commas(row['brand']),
                                  helper.r_commas(row['merchant']),
                                  helper.r_commas(row['productId']),
                                  helper.r_commas(row['keyword']),
                                  helper.r_commas(row['description']),
                                  helper.r_commas(row['category']),
                                  helper.r_commas(row['price']),
                                  helper.r_commas(row['price_sale'])))
            except AttributeError:
                logger.error("Write to file %s, wrong row %s" % (fn, row))
                continue


class DownloadJob(threading.Thread):
    "Job for downloading a given URL."
    def __init__(self, i, q, merchants, api, glob_obj):
        super(DownloadJob, self).__init__()
        self.q = q
        self.i = i
        self.api = api
        self.merch_ids = '|'.join([str(merch) for merch in merchants])
        self.glob_obj = glob_obj
        self.upload_counter = 0


    def run(self):
        while True:
            self._run()

    def _run(self):
        """ Main job process. Here script trying to call API with some filters and save
        a result (API JSON -> python dictionary -> csv file ). If there are no errors,
        result is saved into file output/<brand>.csv """
        brand = self.q.get()
        self.brand = re.sub(r'[\\/:"*?<>|+]', '', brand)
        logger.info("Starting: %s" % self.brand)
        result = self.api.q(query_url=cfg.API_SEARCH,
                            filter_merchs=self.merch_ids,
                            filter_brand=self.brand,
                            page=cfg.PAGE,
                            sphinx_limit=cfg.SPHINX_LIMIT,
                            sphinx_limit_pass=cfg.SPHINX_LIMIT_PASS,
                            relevancy_threshold=cfg.RELEVANCY_THRESHOLD)
        self.glob_obj.request_number += 1
        if len(result['data']) > 9999: # there are more than 10000 items
                                       # - additional requests needed
            result = {'data':[], 'status':'ok'}
            for pricefilter in cfg.PRICE_FILTER:
                result_tmp = self.api.q(query_url=cfg.API_SEARCH,
                                        filter_merchs=self.merch_ids,
                                        filter_brand=self.brand,
                                        page=cfg.PAGE,
                                        sphinx_limit=cfg.SPHINX_LIMIT,
                                        sphinx_limit_pass=cfg.SPHINX_LIMIT_PASS,
                                        relevancy_threshold=cfg.RELEVANCY_THRESHOLD,
                                        filter_price=pricefilter)
                logger.warning("Long request: %s" % self.brand)
                self.glob_obj.request_number += 1
                if result_tmp['data']:
                    logger.info("Writing part of data to file for brand %s" % self.brand)
                    write_to_file(result_tmp['data'], self.brand, self.glob_obj.brands_to_remove)
                    result['status'] = result_tmp['status']
        self.glob_obj.brands_finished += 1
        if result['data']:
            logger.info("Writing data to file for brand %s" % self.brand)
            write_to_file(result['data'], self.brand, self.glob_obj.brands_to_remove)
        else:
            logger.error('%s: zero response' % self.brand)
        if result['status'] == 'error':
            logger.error('%s: error response' % self.brand)
            self.glob_obj.error_list.append(self.brand)
            self.glob_obj.brands_finished -= 1
        logger.info("Finished: %s" % self.brand)
        self.q.task_done() 

class Global_obj(object):
    """define global variables"""
    def __init__(self, brands_to_remove_table):
        super(Global_obj, self).__init__()
        self.request_number = 0
        self.error_list = []
        self.timer = helper.Timer()
        self.total_brands = 0
        self.brands_finished = 0
        self.brands_to_remove = set(helper.return_column_sql(brands_to_remove_table, 'brand_name'))

class Downloader(object):
    """ Main class that run jobs, and restart jobs that finished with errors"""
    def __init__(self, brand_table, merch_table, output_table, glob_obj):
        self.output_fn = output_table
        self.merchs = helper.return_column_sql(merch_table, 'prosperent_id')
        self.brands = sorted(helper.return_column_sql(brand_table, 'brand_name'))
        logger.info("Loaded %d brand names" % len(self.brands))
        self.api = prosperent.api(apikey=cfg.API_KEY, apiurl=cfg.API_URL)
        self.global_obj = glob_obj
        self.global_obj.total_brands = len(self.brands)

    def _dojob(self, brandlist):
        """ creates pool and add jobs into it, after finish return number of jobs """
        q = Queue()
        for i in xrange(cfg.THREADS):
            logger.info("Initializing thread")
            job = DownloadJob(i=i, q=q, merchants=self.merchs, api=self.api, glob_obj=self.global_obj)
            job.setDaemon(True)
            job.start()
        try:
            for brand in brandlist:
                q.put(brand)
            q.join()
        except KeyboardInterrupt as e:
            raise e
    def download(self):
        """ run jobs first time, then restart in case of errors """
        self._dojob(self.brands)
        logger.info("Main loop is finished. Working on errors")
        while self.global_obj.error_list: #working with errors
            logger.error(self.global_obj.error_list)
            self.api.initpool() #re-init api pool
            error_brands = sorted(self.global_obj.error_list[:])
            self.global_obj.error_list = []
            self._dojob(error_brands)


    def download_one(self, bn):
        loggin.info("requesting brand %s" % bn)
        self._dojob([bn])


def main(bn=None):
    logname = 'stage1.log'
    logfn = os.path.join(os.curdir, 'log', logname)
    handl = logging.FileHandler(filename = logfn, mode = 'w')
    fmt = logging.Formatter(u'%(levelname)-8s [%(asctime)s] %(message)s')
    handl.setFormatter(fmt)
    handl.setLevel(logging.INFO)
    logger.addHandler(handl)
    logger.setLevel(logging.INFO)
    logger.info("Stage 1 started")
    lock = helper.Lock('stage1')
    if lock.islocked():
        logger.critical('Already running. Lock file is present')
        sys.exit(1)
    lock.lock()
    helper.clean_dir(cfg.OUT_DIR)
    global_obj = Global_obj(brands_to_remove_table=cfg.BRANDS_TO_REMOVE_TABLE)
    downloader = Downloader(cfg.BRANDS_TABLE, cfg.MERCHS_TABLE, cfg.DB1_FILENAME, global_obj)
    logger.info("Starting download proccess")
    if bn:
        downloader.download_one(bn)
    else:
        downloader.download()
        logger.info("Download process finished")
        logger.info("Starting csv files merge process")
        import merge_db1
        merge_db1.main()
        logger.info("Merging process finished")
        logger.info("Starting db1 upload")
        import populate_db1
        populate_db1.main()
        logger.info("Finished db1 upload")
    lock.unlock()
    logger.info("Stage 1 finished. Exiting")
if __name__ == "__main__":
    try:
        bn = sys.argv[1]
    except IndexError:
        bn = None
    main(bn)
